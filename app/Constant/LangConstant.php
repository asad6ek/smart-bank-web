<?php

namespace App\Constant;

class LangConstant
{
    const  ENGLISH = 'en';
    const RUSSIAN = 'ru';
    const UZBEK = 'uz';

    /**
     * @param $key
     * @return string|bool
     */
    public static function getLang($key): string|bool
    {
        return self::getLangs()[$key] ?? false;
    }

    /**
     * @return string[]
     */
    public static function getLangs(): array
    {
        return [
            self::ENGLISH => "English",
            self::RUSSIAN => 'Русский',
            self::UZBEK => 'O`zbekcha'
        ];
    }

    /**
     * @param $key
     * @return string[]
     */
    public static function getInactiveLangs($key): array
    {
        $langs = self::getLangs();
        unset($langs[$key]);
        return $langs;
    }

}
