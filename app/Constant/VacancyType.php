<?php

namespace App\Constant;

class VacancyType
{
    const   REMOTE = 0;
    const FULL_TIME = 1;
    const HYBRID = 3;

    public static function getNames(): array
    {
        return [
            self::REMOTE => trans('app.remote'),
            self::FULL_TIME => trans('app.full_time'),
            self::HYBRID => trans('app.full_time') . '/' . trans('app.remote')
        ];
    }

    public static function getNameByKey($key)
    {
        return self::getNames()[$key];
    }
}
