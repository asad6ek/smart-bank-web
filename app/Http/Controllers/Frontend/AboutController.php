<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\AboutOfBank;
use App\Models\LegalUser;
use App\Models\Managements;
use App\Models\Vacancy;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class AboutController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        return view('frontend.about.index', ['model' => AboutOfBank::query()->latest()->firstOrFail()]);
    }

    /**
     * @return Factory|View|Application
     */
    public function management(): Factory|View|Application
    {
        return view('frontend.about.management', [
            'managements' => Managements::query()->where('status', 1)->get()
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function partner(): Factory|View|Application
    {
        return view('frontend.about.partner', ['model' => LegalUser::query()->latest()->firstOrFail()]);
    }

    /**
     * @return Factory|View|Application
     */
    public function vacancy(): Factory|View|Application
    {
        return view('frontend.about.vacancy', ['vacancies' => Vacancy::query()->where('status', 1)->get()]);
    }
}
