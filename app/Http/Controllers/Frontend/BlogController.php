<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class BlogController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        $latest = Blog::query()->latest()->first();
        $models = Blog::query()
            ->where('id', '!=', $latest->id ?? null)->where('status', 1)
            ->paginate(10);
        return view('frontend.blog.index', [
            'blogs' => $models,
            'latest' => $latest,
        ]);
    }


    public function view(int $id): Factory|View|Application
    {
        if ($model = Blog::query()->where('status', 1)->where('id', $id)->first()) {
            $model->update(['review_count' => $model->review_count + 1]);
            return view('frontend.blog.view', [
                'model' => $model,
                'blogs' => Blog::query()->where('id', '!=', $id)
                    ->where('status', 1)->limit(10)->get()
            ]);
        }
        abort(404);
    }
}
