<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\AboutOfBank;
use App\Models\Blog;
use App\Models\Card;
use App\Models\Contact;
use App\Models\Contribution;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\File;

class SiteController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        return view('frontend.index', [
            'blogs' => Blog::query()->where('status', 1)->limit(9)->get()
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function card(): Factory|View|Application
    {
        return view('frontend.card', [
            'models' => Card::query()->where('status', 1)->get(),
            'card' => Card::query()->where('status', 1)->where('type', Card::IS_CARD)->latest()->first()
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function deposit(): Factory|View|Application
    {
        return view('frontend.deposit', [
            'models' => Contribution::query()->where('status', 1)->get(),
            'about_of_bank' => AboutOfBank::query()->latest()->first()
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function contact(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'full_name' => ['required', 'min:3'],
            'email' => ['required', 'email'],
            'phone_number' => ['required', 'string', 'min:9', 'max:9'],
            'file_path' => ['required', File::types(['pdf', 'docx', 'doc'])->max(5 * 1024)]
        ]);
        $file = $request->file('file_path');
        $fileName = uniqid() . time() . '.' . $file->extension();
        $file->storeAs('applications', $fileName, ['disk' => 'public']);
        $data['file_path'] = 'applications/' . $fileName;
        Contact::query()->create($data);
        return redirect()->back()->with('success', 'Created successfully!');
    }
}
