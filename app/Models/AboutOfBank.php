<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AboutOfBank
 *
 * @property int $id
 * @property string $main_tasks_uz
 * @property string $main_tasks_ru
 * @property string $main_tasks_en
 * @property string $bank_mission_en
 * @property string $bank_mission_ru
 * @property string $bank_mission_uz
 * @property string $short_bank_mission_en
 * @property string $short_bank_mission_ru
 * @property string $short_bank_mission_uz
 * @property string $file_path
 * @property string $image_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereBankMissionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereBankMissionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereBankMissionUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereMainTasksEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereMainTasksRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereMainTasksUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereShortBankMissionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereShortBankMissionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereShortBankMissionUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutOfBank whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AboutOfBank extends Model
{
    use HasFactory;
    protected $table = 'about_of_bank';
}
