<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Blog
 *
 * @property int $id
 * @property string $title_uz
 * @property string $title_ru
 * @property string $title_en
 * @property string $short_description_uz
 * @property string $short_description_ru
 * @property string $short_description_en
 * @property string $description_uz
 * @property string $description_ru
 * @property string $description_en
 * @property string $image_path_1
 * @property string $image_path_2
 * @property bool $status
 * @property int $review_count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescriptionUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereImagePath1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereImagePath2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereReviewCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereShortDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereShortDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereShortDescriptionUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitleRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitleUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'title_uz',
        'title_ru',
        'title_en',
        'short_description_uz',
        'short_description_ru',
        'short_description_en',
        'description_en',
        'description_ru',
        'description_uz',
        'image_path_1',
        'image_path_2',
        'status',
        'review_count',
    ];
}
