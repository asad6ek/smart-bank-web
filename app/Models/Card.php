<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\ArrayShape;

/**
 * App\Models\Card
 *
 * @property int $id
 * @property string $value_uz
 * @property string $value_ru
 * @property string $value_en
 * @property string|null $label_uz
 * @property string|null $label_ru
 * @property string|null $label_en
 * @property string|null $description_uz
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property bool $status
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Card whereCreatedAt($value)
 * @method static Builder|Card whereDescriptionEn($value)
 * @method static Builder|Card whereDescriptionRu($value)
 * @method static Builder|Card whereDescriptionUz($value)
 * @method static Builder|Card whereId($value)
 * @method static Builder|Card whereLabelEn($value)
 * @method static Builder|Card whereLabelRu($value)
 * @method static Builder|Card whereLabelUz($value)
 * @method static Builder|Card whereStatus($value)
 * @method static Builder|Card whereType($value)
 * @method static Builder|Card whereUpdatedAt($value)
 * @method static Builder|Card whereValueEn($value)
 * @method static Builder|Card whereValueRu($value)
 * @method static Builder|Card whereValueUz($value)
 * @method static Builder|Card newModelQuery()
 * @method static Builder|Card newQuery()
 * @method static Builder|Card query()
 * @mixin \Eloquent
 */
class Card extends Model
{
    use HasFactory;

    public const  IS_CHECKBOX = 1;
    public const  IS_CARD = 2;
    public const  IS_ADVANTAGE = 3;
    public const  IS_INF_LIST = 4;

    protected $fillable = [
        'value_uz',
        'value_ru',
        'value_en',
        'label_uz',
        'label_ru',
        'label_en',
        'description_uz',
        'description_ru',
        'description_en',
        'status',
        'status',
    ];

    #[ArrayShape([self::IS_CHECKBOX => "string", self::IS_CARD => "string", self::IS_ADVANTAGE => "string", self::IS_INF_LIST => "string"])]
    public static function getStatusList(): array
    {
        return [
            self::IS_CHECKBOX => 'Is Checkbox list',
            self::IS_CARD => 'Is card',
            self::IS_ADVANTAGE => 'Is advantage list',
            self::IS_INF_LIST => 'Is short info list',
        ];
    }
}
