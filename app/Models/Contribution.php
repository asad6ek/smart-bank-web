<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Contribution
 *
 * @property int $id
 * @property string $label_uz
 * @property string $label_ru
 * @property string $label_en
 * @property string $value_uz
 * @property string $value_ru
 * @property string $value_en
 * @property bool $status
 * @property bool $is_checkbox
 * @property bool $is_about_deposit
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Contribution newModelQuery()
 * @method static Builder|Contribution newQuery()
 * @method static Builder|Contribution query()
 * @method static Builder|Contribution whereCreatedAt($value)
 * @method static Builder|Contribution whereId($value)
 * @method static Builder|Contribution whereIsAboutDeposit($value)
 * @method static Builder|Contribution whereIsCheckbox($value)
 * @method static Builder|Contribution whereLabelEn($value)
 * @method static Builder|Contribution whereLabelRu($value)
 * @method static Builder|Contribution whereLabelUz($value)
 * @method static Builder|Contribution whereStatus($value)
 * @method static Builder|Contribution whereUpdatedAt($value)
 * @method static Builder|Contribution whereValueEn($value)
 * @method static Builder|Contribution whereValueRu($value)
 * @method static Builder|Contribution whereValueUz($value)
 * @mixin \Eloquent
 */
class Contribution extends Model
{
    use HasFactory;

    protected $fillable = [
        'label_uz',
        'label_ru',
        'label_en',
        'value_uz',
        'value_ru',
        'value_en',
        'status',
        'is_checkbox',
        'is_about_deposit',
    ];
}
