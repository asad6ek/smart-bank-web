<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mostafaznv\NovaCkEditor\ImageStorage;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $name
 * @property string $file
 * @property string $disk
 * @property string $mime
 * @property int $width
 * @property int $height
 * @property string $size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $url
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereMime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereWidth($value)
 * @mixin \Eloquent
 */
class Image extends Model
{
    protected $fillable = [
        'name', 'file', 'disk', 'mime', 'width', 'height', 'size'
    ];

    public function setNameAttribute($value)
    {
        if ($value) {
            $this->attributes['name'] = trim($value);
        }
        else if ($file = request()->file('file')) {
            $this->attributes['name'] = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        }
    }

    public function getUrlAttribute(): string
    {
        return ImageStorage::make($this->attributes['disk'])->url($this->attributes['file']);
    }

    public function getSizeAttribute(): string
    {
        return ImageStorage::bytesForHumans($this->attributes['size']);
    }
}
