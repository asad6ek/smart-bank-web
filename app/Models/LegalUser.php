<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LegalUser
 *
 * @property int $id
 * @property string|null $file_path_1
 * @property string|null $file_path_2
 * @property string|null $file_path_3
 * @property string $label_1_uz
 * @property string $label_1_ru
 * @property string $label_1_en
 * @property string $label_2_uz
 * @property string $label_2_ru
 * @property string $label_2_en
 * @property string $label_3_uz
 * @property string $label_3_ru
 * @property string $label_3_en
 * @property string $description_1_uz
 * @property string $description_1_ru
 * @property string $description_1_en
 * @property string $description_2_uz
 * @property string $description_2_ru
 * @property string $description_2_en
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription1En($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription1Ru($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription1Uz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription2En($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription2Ru($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereDescription2Uz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereFilePath1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereFilePath2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereFilePath3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel1En($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel1Ru($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel1Uz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel2En($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel2Ru($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel2Uz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel3En($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel3Ru($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereLabel3Uz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LegalUser extends Model
{
    use HasFactory;
}
