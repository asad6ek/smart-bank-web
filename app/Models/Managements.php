<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Managements
 *
 * @property int $id
 * @property string $image_path_1
 * @property string $image_path_2
 * @property string $full_name
 * @property string $short_bio_uz
 * @property string $short_bio_ru
 * @property string $short_bio_en
 * @property string|null $bio_uz
 * @property string|null $bio_ru
 * @property string|null $bio_en
 * @property bool $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Managements newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Managements newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Managements query()
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereBioEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereBioRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereBioUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereImagePath1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereImagePath2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereShortBioEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereShortBioRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereShortBioUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Managements whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Managements extends Model
{
    use HasFactory;
}
