<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Vacancy
 *
 * @property int $id
 * @property string $image_path
 * @property string $title_en
 * @property string $title_ru
 * @property string $title_uz
 * @property int|null $from
 * @property int|null $to
 * @property int $type_of_work
 * @property string|null $description_en
 * @property string|null $description_ru
 * @property string|null $description_uz
 * @property bool $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereDescriptionUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTitleRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTitleUz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereTypeOfWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vacancy extends Model
{
    use HasFactory;
}
