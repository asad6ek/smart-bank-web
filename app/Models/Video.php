<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mostafaznv\Larupload\LaruploadEnum;
use Mostafaznv\Larupload\Storage\Attachment;
use Mostafaznv\Larupload\Traits\Larupload;
use Exception;

/**
 * App\Models\Video
 *
 * @property int $id
 * @property string $name
 * @property string|null $file_file_name
 * @property int|null $file_file_size
 * @property string|null $file_file_type
 * @property string|null $file_file_mime_type
 * @property int|null $file_file_width
 * @property int|null $file_file_height
 * @property int|null $file_file_duration
 * @property string|null $file_file_dominant_color
 * @property string|null $file_file_format
 * @property string|null $file_file_cover
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Mostafaznv\Larupload\Models\LaruploadFFMpegQueue|null $laruploadQueue
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mostafaznv\Larupload\Models\LaruploadFFMpegQueue[] $laruploadQueues
 * @property-read int|null $larupload_queues_count
 * @method static \Illuminate\Database\Eloquent\Builder|Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video query()
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileDominantColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereFileFileWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Video extends Model
{
    use Larupload;

    protected $fillable = ['name', 'file', 'disk'];

    protected static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            $hasLaruploadTrait = method_exists(self::class, 'bootLarupload');

            if (!$model->name) {
                $name = $hasLaruploadTrait ? $model->file->meta('name') : $model->file;

                $model->name = pathinfo($name, PATHINFO_FILENAME);
            }
        });
    }

    /**
     * @throws Exception
     */
    public function attachments(): array
    {
        return [
            Attachment::make('file')
                ->disk('video')
                ->namingMethod(LaruploadEnum::HASH_FILE_NAMING_METHOD)
                ->coverStyle(852, 480, LaruploadEnum::AUTO_STYLE_MODE),
        ];
    }
}
