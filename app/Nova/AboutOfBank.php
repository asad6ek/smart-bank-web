<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Http\Requests\NovaRequest;
use Mostafaznv\NovaCkEditor\CkEditor;

class AboutOfBank extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\AboutOfBank::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        if ($request->resourceId) {
            $model = \App\Models\AboutOfBank::query()->where('id', $request->resourceId)->first();
        }
        return [
            ID::make()->sortable(),
            File::make('Лицензия банка', 'file_path')->disk('public')
                ->creationRules('mimes:pdf')
                ->updateRules('mimes:pdf')
                ->rules(Rule::requiredIf(!$model->file_path), 'mimes:pdf'),

            CkEditor::make('Main tasks uz', 'main_tasks_uz')->stacked()
                ->rules('required'),
            CkEditor::make('Main tasks ru', 'main_tasks_ru')->stacked()
                ->rules('required'),
            CkEditor::make('Main tasks en', 'main_tasks_en')->stacked()
                ->rules('required'),

            CkEditor::make('Short bank mission uz', 'short_bank_mission_uz')->stacked()
                ->rules('required'),
            CkEditor::make('Short bank mission ru', 'short_bank_mission_ru')->stacked()
                ->rules('required'),
            CkEditor::make('Short bank mission en', 'short_bank_mission_en')->stacked()
                ->rules('required'),

            CkEditor::make('Bank mission uz', 'bank_mission_uz')->stacked()
                ->rules('required'),
            CkEditor::make('Bank mission ru', 'bank_mission_ru')->stacked()
                ->rules('required'),
            CkEditor::make('Bank mission en', 'bank_mission_en')->stacked()
                ->rules('required'),

            Image::make('Image path', 'image_path')->disk('public'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
