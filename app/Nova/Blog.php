<?php

namespace App\Nova;

use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Mostafaznv\NovaCkEditor\CkEditor;

class Blog extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static string $model = \App\Models\Blog::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title_uz',
        'title_ru',
        'title_en',
        'short_description_en',
        'short_description_ru',
        'short_description_uz',
        'description_uz',
        'description_ru',
        'description_en',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title uz', 'title_uz')->sortable()->rules('required', 'max:255')
                ->hideFromIndex(function () {
                    return app()->getLocale() !== 'uz';
                }),
            Text::make('Title ru', 'title_ru')->sortable()->rules('required', 'max:255')
                ->hideFromIndex(function () {
                    return app()->getLocale() !== 'ru';
                }),
            Text::make('Title en', 'title_en')->sortable()->rules('required', 'max:255')
                ->hideFromIndex(function () {
                    return app()->getLocale() !== 'en';
                }),
            Image::make('Image path 1', 'image_path_1')->disk('public')
                ->rules(Rule::requiredIf(!$request->blog))->hideFromIndex(),
            Image::make('Image path 2', 'image_path_2')->disk('public')
                ->rules(Rule::requiredIf(!$request->blog))->hideFromIndex(),
            Boolean::make('Status', 'status'),

            CkEditor::make('Short description uz', 'short_description_uz')->stacked()
                ->hideFromIndex()->rules('required'),
            CkEditor::make('Short description ru', 'short_description_ru')->stacked()
                ->rules('required')->hideFromIndex(),
            CkEditor::make('Short description en', 'short_description_en')->stacked()
                ->rules('required')->hideFromIndex(),
            CkEditor::make('Description uz', 'description_uz')->stacked()
                ->rules('required')->hideFromIndex(),
            CkEditor::make('Description ru', 'description_ru')->stacked()
                ->rules('required')->hideFromIndex(),
            CkEditor::make('Description en', 'description_en')->stacked()
                ->rules('required')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
