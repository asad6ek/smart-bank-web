<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Card extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Card::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Value uz ', 'value_uz')->rules('required')->hideFromIndex(function () {
                return app()->getLocale() !=='uz';}),
            Text::make('Value ru ', 'value_ru')->rules('required')
                ->hideFromIndex(function () {
                return app()->getLocale() !=='ru';}),
            Text::make('Value en ', 'value_en')->rules('required')->hideFromIndex(function () {
                return app()->getLocale() !=='en';}),
            Text::make('Label uz', 'label_uz')->rules('max:255')->hideFromIndex(),
            Text::make('Label ru', 'label_ru')->rules('max:255')->hideFromIndex(),
            Text::make('Label en', 'label_en')->rules('max:255')->hideFromIndex(),
            Text::make('Description uz ', 'description_uz')->rules('required')->hideFromIndex(),
            Text::make('Description ru ', 'description_ru')->rules('required')->hideFromIndex(),
            Text::make('Description en ', 'description_en')->rules('required')->hideFromIndex(),
            Select::make('Type', 'type')->options(\App\Models\Card::getStatusList())->displayUsingLabels(),
            Boolean::make('Status', 'status'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
