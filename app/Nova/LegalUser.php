<?php

namespace App\Nova;

use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class LegalUser extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static string $model = \App\Models\LegalUser::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        $legal = \App\Models\LegalUser::query()->where('id', $request->resourceId)->firstOrFail();
        return [
            ID::make()->sortable(),
            File::make('File 1', 'file_path_1')->disk('public')
                ->creationRules('mimes:pdf')
                ->updateRules('mimes:pdf')
                ->rules(Rule::requiredIf(!$legal->file_path_1), 'mimes:pdf'),
            File::make('File 2', 'file_path_2')->disk('public')
                ->creationRules('mimes:pdf')
                ->updateRules('mimes:pdf')
                ->rules(Rule::requiredIf(!$legal->file_path_2), 'mimes:pdf'),
            File::make('File 3', 'file_path_3')->disk('public')
                ->creationRules('mimes:pdf')
                ->updateRules('mimes:pdf')
                ->rules(Rule::requiredIf(!$legal->file_path_3), 'mimes:pdf'),

            Text::make('File 1 label uz', 'label_1_uz')->rules('required'),
            Text::make('File 1 label ru', 'label_1_ru')->rules('required'),
            Text::make('File 1 label  en', 'label_1_en')->rules('required'),

            Text::make('File 2 label uz', 'label_2_uz')->rules('required'),
            Text::make('File 2 label ru', 'label_2_ru')->rules('required'),
            Text::make('File 2 label en', 'label_2_en')->rules('required'),

            Text::make('File 3 label uz', 'label_3_uz')->rules('required'),
            Text::make('File 3 label ru', 'label_3_ru')->rules('required'),
            Text::make('File 3 label en', 'label_3_en')->rules('required'),

            Text::make('Description 1 uz', 'description_1_uz')->rules('required'),
            Text::make('Description 1 ru', 'description_1_ru')->rules('required'),
            Text::make('Description 1 en', 'description_1_en')->rules('required'),

            Text::make('Description 2 uz', 'description_2_uz')->rules('required'),
            Text::make('Description 2 ru', 'description_2_ru')->rules('required'),
            Text::make('Description 2 en', 'description_2_en')->rules('required'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
