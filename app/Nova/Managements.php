<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Mostafaznv\NovaCkEditor\CkEditor;

class Managements extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Managements::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'full_name',
        'short_bio_uz',
        'short_bio_ru',
        'short_bio_en',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Full name', 'full_name')->sortable()->rules('required', 'max:255'),
            Text::make('Short bio uz', 'short_bio_uz')->rules('required')->hideFromIndex(),
            Text::make('Short bio ru', 'short_bio_ru')->rules('required')->hideFromIndex(),
            Text::make('Short bio en', 'short_bio_en')->rules('required')->hideFromIndex(),
            Boolean::make('Status', 'status'),
            Image::make('Image path 1', 'image_path_1')->disk('public')->rules(Rule::requiredIf(!$request->management)),
            Image::make('Image path 2', 'image_path_2')->disk('public')->rules(Rule::requiredIf(!$request->management))
                ->hideFromIndex(),
            CkEditor::make('Bio uz', 'bio_uz')->stacked()->hideFromIndex(),
            CkEditor::make('Bio ru', 'bio_ru')->stacked()->hideFromIndex(),
            CkEditor::make('Bio en', 'bio_en')->stacked()->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
