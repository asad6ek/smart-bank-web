<?php

namespace App\Nova;

use App\Constant\VacancyType;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Mostafaznv\NovaCkEditor\CkEditor;

class Vacancy extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Vacancy::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title uz', 'title_uz')
                ->sortable()->rules('required', 'max:255')->hideFromIndex(function () {
                    return app()->getLocale() !== 'uz';
                }),
            Text::make('Title ru', 'title_ru')
                ->sortable()->rules('required', 'max:255')->hideFromIndex(function () {
                    return app()->getLocale() !== 'ru';
                }),
            Text::make('Title en', 'title_en')
                ->sortable()->rules('required', 'max:255')->hideFromIndex(function () {
                    return app()->getLocale() !== 'en';
                }),
            Text::make('From', 'from')->sortable()->rules('nullable', 'min:0', 'integer'),
            Text::make('To', 'to')->sortable()->rules('nullable', 'min:0', 'integer'),
            Select::make('Type of work')->options(VacancyType::getNames())->displayUsingLabels(),
            Image::make('Image path', 'image_path')->disk('public'),
            Boolean::make('Status', 'status'),
            CkEditor::make('Description uz', 'description_uz')->stacked()->hideFromIndex(),
            CkEditor::make('Description ru', 'description_ru')->stacked()->hideFromIndex(),
            CkEditor::make('Description en', 'description_en')->stacked()->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
