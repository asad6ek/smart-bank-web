<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AboutOfBankPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(): bool
    {
        return false;
    }

    public function view(): bool
    {
        return true;
    }

    public function edit(): bool
    {
        return true;
    }
    public function update(): bool
    {
        return true;
    }
}
