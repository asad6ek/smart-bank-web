<?php

namespace App\Providers;

use App\Models\AboutOfBank;
use App\Models\LegalUser;
use App\Nova\Blog;
use App\Nova\Card;
use App\Nova\Contact;
use App\Nova\Contribution;
use App\Nova\Dashboards\Main;
use App\Nova\Managements;
use App\Nova\SocialNetwork;
use App\Nova\User;
use App\Nova\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Badge;
use Laravel\Nova\Menu\MenuItem;
use Laravel\Nova\Menu\MenuSection;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

class NovaServiceProvider extends NovaApplicationServiceProvider
{

    public static string $model = 'App\\Models\\User';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Nova::mainMenu(function (Request $request) {
            $about_id = AboutOfBank::query()->latest()->first()->id;
            $legal_id = LegalUser::query()->latest()->first()->id;
            return [
                MenuSection::dashboard(Main::class)->icon('chart-bar'),
                MenuSection::resource(User::class)->icon('user'),
                MenuSection::resource(Blog::class)->icon('newspaper'),
                MenuSection::resource(Vacancy::class),
                MenuSection::resource(Managements::class),
                MenuSection::resource(SocialNetwork::class),
                MenuSection::resource(Contribution::class),
                MenuSection::resource(Card::class),
                MenuSection::resource(Contact::class)
                    ->withBadgeIf(Badge::make('New!'), 'info', fn() => \App\Models\Contact::query()->where('is_read',0)->count() > 0),
                MenuSection::make('About of bank')
                    ->path('/resources/about-of-banks/' . $about_id)
                    ->icon('document-text'),
                MenuSection::make('Legal user')
                    ->path('/resources/legal-users/' . $legal_id)
                    ->icon('document-text'),
                MenuItem::externalLink('Translation', '/admin/translations')->openInNewTab(),
            ];
        });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the dashboards that should be listed in the Nova sidebar.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [
            new Main,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
