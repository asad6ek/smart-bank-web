<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_of_bank', function (Blueprint $table) {
            $table->id();
            $table->text('main_tasks_uz');
            $table->text('main_tasks_ru');
            $table->text('main_tasks_en');
            $table->text('bank_mission_en');
            $table->text('bank_mission_ru');
            $table->text('bank_mission_uz');
            $table->text('short_bank_mission_en');
            $table->text('short_bank_mission_ru');
            $table->text('short_bank_mission_uz');
            $table->string('file_path')->nullable();
            $table->string('image_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_of_bank');
    }
};
