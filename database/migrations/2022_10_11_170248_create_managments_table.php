<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managements', function (Blueprint $table) {
            $table->id();
            $table->string('image_path_1');
            $table->string('image_path_2');
            $table->string('full_name');
            $table->text('short_bio_uz');
            $table->text('short_bio_ru');
            $table->text('short_bio_en');
            $table->text('bio_uz')->nullable();
            $table->text('bio_ru')->nullable();
            $table->text('bio_en')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_council')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managements');
    }
};
