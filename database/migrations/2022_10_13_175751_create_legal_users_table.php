<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_users', function (Blueprint $table) {
            $table->id();
            $table->string('file_path_1')->nullable();
            $table->string('file_path_2')->nullable();
            $table->string('file_path_3')->nullable();
            $table->text('label_1_uz');
            $table->text('label_1_ru');
            $table->text('label_1_en');
            $table->text('label_2_uz');
            $table->text('label_2_ru');
            $table->text('label_2_en');
            $table->text('label_3_uz');
            $table->text('label_3_ru');
            $table->text('label_3_en');
            $table->text('description_1_uz');
            $table->text('description_1_ru');
            $table->text('description_1_en');
            $table->text('description_2_uz');
            $table->text('description_2_ru');
            $table->text('description_2_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_users');
    }
};
