<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributions', function (Blueprint $table) {
            $table->id();
            $table->string('label_uz')->nullable();
            $table->string('label_ru')->nullable();
            $table->string('label_en')->nullable();
            $table->string('value_uz');
            $table->string('value_ru');
            $table->string('value_en');
            $table->boolean('status')->default(true);
            $table->boolean('is_checkbox')->default(false);
            $table->boolean('is_about_deposit')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributions');
    }
};
