<?php

namespace Database\Seeders;

use App\Models\AboutOfBank;
use Illuminate\Database\Seeder;

class AboutOfBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AboutOfBank::query()->firstOrCreate([
            'main_tasks_ru' => 'активное кредитование крупных инвестиционных проектов в приоритетных секторах экономики с привлечением зарубежных кредитных линий, ресурсов на международных рынках капитала, средств иностранных инвесторов; внедрение инновационных финансовых продуктов для повышения качества и скорости обслуживания; предоставление банковских услуг субъектам малого бизнеса и частного предпринимательства, а также финансовых консультаций и информационной поддержки в вопросах внешнеэкономической деятельности; развитие розничного банковского обслуживания населения, внедрение новых видов банковских услуг, в том числе с использованием информационно-коммуникационных технологий; диверсификация источников финансирования деятельности банка в первую очередь за счет мобилизации долгосрочных средств в национальной валюте; развитие продуктов и расширение географии бизнеса торгово-экспортного финансирования, а также поддержка программ развития экспортного потенциала республики, предоставление консалтинговых услуг экспортерам; оказание услуг инвестиционного банкинга корпоративным клиентам, в том числе по выпуску и размещению ценных бумаг на внутреннем и внешних рынках.',
        ], [
            'main_tasks_ru' => 'активное кредитование крупных инвестиционных проектов в приоритетных секторах экономики с привлечением зарубежных кредитных линий, ресурсов на международных рынках капитала, средств иностранных инвесторов; внедрение инновационных финансовых продуктов для повышения качества и скорости обслуживания; предоставление банковских услуг субъектам малого бизнеса и частного предпринимательства, а также финансовых консультаций и информационной поддержки в вопросах внешнеэкономической деятельности; развитие розничного банковского обслуживания населения, внедрение новых видов банковских услуг, в том числе с использованием информационно-коммуникационных технологий; диверсификация источников финансирования деятельности банка в первую очередь за счет мобилизации долгосрочных средств в национальной валюте; развитие продуктов и расширение географии бизнеса торгово-экспортного финансирования, а также поддержка программ развития экспортного потенциала республики, предоставление консалтинговых услуг экспортерам; оказание услуг инвестиционного банкинга корпоративным клиентам, в том числе по выпуску и размещению ценных бумаг на внутреннем и внешних рынках.',
            'main_tasks_uz' => 'xorijiy kredit liniyalari, xalqaro kapital bozorlaridagi resurslar, xorijiy investorlar mablag‘larini jalb qilgan holda iqtisodiyotning ustuvor tarmoqlaridagi yirik investitsiya loyihalarini faol kreditlash; xizmat ko‘rsatish sifati va tezligini oshirish uchun innovatsion moliyaviy mahsulotlarni joriy etish; kichik biznes va xususiy tadbirkorlik subyektlariga bank xizmatlari ko‘rsatish, shuningdek, tashqi iqtisodiy faoliyat masalalari bo‘yicha moliyaviy maslahat va axborot ta’minoti; aholi uchun chakana bank xizmatlarini rivojlantirish, bank xizmatlarining yangi turlarini, shu jumladan, axborot-kommunikatsiya texnologiyalaridan foydalangan holda joriy etish; bank faoliyatini moliyalashtirish manbalarini, birinchi navbatda, milliy valyutadagi uzoq muddatli mablag‘larni safarbar etish hisobiga diversifikatsiya qilish; mahsulot ishlab chiqarishni rivojlantirish va savdo biznesi geografiyasini kengaytirish va eksportni moliyalashtirish, shuningdek, respublika eksport salohiyatini rivojlantirish dasturlarini qo‘llab-quvvatlash, eksport qiluvchilarga konsalting xizmatlarini ko‘rsatish; korporativ mijozlarga investitsion bank xizmatlarini ko‘rsatish, shu jumladan ichki va tashqi bozorlarda qimmatli qog‘ozlarni chiqarish va joylashtirish.',
            'main_tasks_en' => 'active lending to large investment projects in priority sectors of the economy with the attraction of foreign credit lines, resources in international capital markets, funds of foreign investors; introduction of innovative financial products to improve the quality and speed of service; provision of banking services to small businesses and private entrepreneurship, as well as financial advice and information support in matters of foreign economic activity; development of retail banking services for the population, introduction of new types of banking services, including using information and communication technologies; diversification of sources of financing for the bank\'s activities, primarily through the mobilization of long-term funds in the national currency; development of products and expansion of the geography of the business of trade and export financing, as well as support for programs to develop the export potential of the republic, provision of consulting services to exporters; provision of investment banking services to corporate clients, including the issue and placement of securities in the domestic and foreign markets.',
            'short_bank_mission_ru' => 'Миссия Агробанка заключается в обеспечении устойчивого развития аграрного, сельскохозяйственного сектора, углублении экономических реформ в сельском хозяйстве страны, всяческой поддержке фермерского движения, его укреплении материальной и финансовой базы, внедрении современной техники и технологий по производству и переработке качественных, конкурентоспособных сельскохозяйственных товаров, а также предоставлении широкого спектра банковских услуг направленных на наполнение внутреннего рынка местными потребительскими товарами.',
            'short_bank_mission_en' => 'The mission of Agrobank is to ensure the sustainable development of the agrarian and agricultural sector, deepen economic reforms in the country\'s agriculture, support the farming movement in every possible way, strengthen its material and financial base, introduce modern equipment and technologies for the production and processing of high-quality, competitive agricultural products, as well as provide a wide range of banking services aimed at filling the domestic market with local consumer goods.',
            'short_bank_mission_uz' => '“Agrobank”ning vazifasi agrar va agrar sektorning barqaror rivojlanishini ta’minlash, mamlakatimiz qishloq xo‘jaligida iqtisodiy islohotlarni chuqurlashtirish, fermerlik harakatini har tomonlama qo‘llab-quvvatlash, uning moddiy-moliyaviy bazasini mustahkamlash, ishlab chiqarishga zamonaviy texnika va texnologiyalarni joriy etishdan iborat. sifatli, raqobatbardosh qishloq xo‘jaligi mahsulotlarini qayta ishlash, shuningdek, ichki bozorni mahalliy iste’mol tovarlari bilan to‘ldirishga qaratilgan keng ko‘lamli bank xizmatlarini ko‘rsatish.',
            'bank_mission_ru' => '   <p>Уважаемые акционеры! Дорогие дамы и господа!</p>
                    <p>Прошедший год ознаменовался ускоренным ростом
                        экономики нашей страны, осуществлена большая работа по дальнейшему повышению ликвидности и
                        укреплению стабильности банков. </p>
                    <p>Достигнуты положительные изменения в кредитной и инвестиционной
                        деятельности банков. Год от года растёт общий объем кредитов, направленных в реальный сектор
                        экономики. В этих положительных изменениях немаловажную роль сыграли указы и постановления
                        Президента, которые послужили программными документами развития.</p>
                    <p>Мы с гордостью и уверенностью
                        можем сказать, что в росте экономических показателей страны есть доля и акционерного
                        коммерческого банка «Агробанк», и это не будет преувеличением. </p>
                    <p>В течении прошедшего 2017 года
                        Агробанком, осуществлены последовательные меры по выполнению установленных задач, вытекающих из
                        приоритетных направлений экономического развития страны, которые направлены на дальнейшее
                        повышение капитализации иинвестиционной активности банка, дальнейшее расширение объема
                        кредитования, направленного на модернизацию производства, создание новых рабочих мест. </p>
                    <p>В результате предпринятых мер по финансовому укреплению банка Уставный капитал банка достиг
                        объёма
                        1335,6 миллиарда сумов. </p>
                    <p>Ресурсная база банка в отчётном году составила 4392,7 миллиарда сумов,
                        активы составили 4981,5 миллиарда сумов. В отчётном году банк продолжил политику наращивания
                        привлечения временно свободных денежных средств населения во вклады, в результате чего на
                        1-января 2018 года объем депозитов составил 1659.5 миллиарда сумов. Кроме того, размещены
                        депозитные сертификаты в объёме 135,2 миллиард сумов. При осуществлении финансирования и
                        кредитования сельского хозяйства республики, основное внимание было уделено финансовой поддержке
                        аграрного комплекса, увеличению промышленного производства, заготовке и переработке
                        сельскохозяйственной продукции, финансированию фермерских хозяйств. Укреплены взаимовыгодные
                        связи с международными рейтинговыми агентствами и международными финансовыми институтами. В
                        текущем финансовом году банк продолжит исполнение своего стратегического бизнес-плана,
                        направленного на дальнейшее обеспечение его стабильности и эффективности деятельности, в
                        соответствии с международными нормами и стандартами. </p>
                    <p>Будет расширять виды и объёмы оказываемых
                        услуг, оказывать всемерную финансовую поддержку предприятиям реального сектора экономики,
                        развивать малый бизнес и частное предпринимательство, прилагать все свои усилия и возможности
                        для укрепления материально-технической базы дехканских и фермерских хозяйств. Председатель
                        Правления</p>',
            'bank_mission_uz' => '<p>Hurmatli aksiyadorlar! Hurmatli xonimlar va janoblar!</p>
                    <p>O\'tgan yil jadal o\'sish bilan ajralib turdi
                        mamlakatimiz iqtisodiyoti, likvidlilikni yanada oshirish va
                        banklar barqarorligini mustahkamlash. </p>
                    <p>Kredit va investitsiyalar sohasida ijobiy o\'zgarishlarga erishildi
                        bank faoliyati. Real sektorga yo‘naltirilgan kreditlarning umumiy hajmi yildan-yilga ortib bormoqda
                        iqtisodiyot. Bu ijobiy o‘zgarishlarda farmon va qarorlar muhim o‘rin tutdi.
    Rivojlanish uchun dasturiy hujjatlar boʻlib xizmat qilgan Prezident.</p>
                    <p>Biz faxrlanamiz va o\'zimizga ishonamiz
                        Aytishimiz mumkinki, mamlakat iqtisodiy ko\'rsatkichlari o\'sishida o\'z mablag\'lari hissasi bor
                        “Agrobank” tijorat banki va bu mubolag\'a emas. </p>
                    <p>O\'tgan 2017 yil davomida
                        Agrobank tomonidan belgilangan vazifalarni bajarish yuzasidan izchil chora-tadbirlar amalga oshirildi
                        mamlakat iqtisodiyotini rivojlantirishning ustuvor yo‘nalishlari bo‘lib, ular bundan keyin ham yo‘naltirilgan
                        bank kapitallashuvi va investitsiya faolligini oshirish, hajmni yanada kengaytirish
                        ishlab chiqarishni modernizatsiya qilish, yangi ish o‘rinlari yaratishga qaratilgan kreditlash. </p>
                    <p>Bankni moliyaviy mustahkamlash bo\'yicha ko\'rilgan chora-tadbirlar natijasida bankning ustav kapitaliga erishildi
                        hajmi
                        1335,6 mlrd.so‘m. </p>
                    <p>Bankning resurs bazasi hisobot yilida 4392,7 mlrd.
                        aktivlari 4981,5 mlrd.so\'mni tashkil etdi. Hisobot yilida bank o\'sish siyosatini davom ettirdi
                        aholining vaqtincha bo\'sh pul mablag\'larini depozitlarga jalb qilish, buning natijasida
                        2018-yil 1-yanvar holatiga ko‘ra depozitlar hajmi 1659,5 mlrd. Bundan tashqari, joylashtirilgan
                        so\'mlik depozit sertifikatlari 135,2 mlrd. Moliyalashtirishda va
                        respublika qishloq xo\'jaligini kreditlashda asosiy e\'tibor moliyaviy qo\'llab-quvvatlashga qaratildi
                        qishloq xo\'jaligi kompleksi, sanoat ishlab chiqarishni ko\'paytirish, hosilni yig\'ish va qayta ishlash
                        qishloq xo\'jaligi mahsulotlari, fermer xo\'jaliklarini moliyalashtirish. O\'zaro manfaatli
                        xalqaro reyting agentliklari va xalqaro moliya institutlari bilan aloqalar. DA
                        joriy moliyaviy yilda bank o\'zining strategik biznes-rejasini amalga oshirishda davom etadi;
                        uning barqarorligi va samaradorligini yanada ta\'minlashga qaratilgan, in
                        xalqaro normalar va standartlarga muvofiq. </p>
                    <p>Renderlash turlari va koʻlamini kengaytiradi
                        xizmatlar koʻrsatish, iqtisodiyotning real sektori korxonalarini har tomonlama moliyaviy qoʻllab-quvvatlash;
                        kichik biznes va xususiy tadbirkorlikni rivojlantirish, barcha kuch va imkoniyatlarni ishga solish
                        dehqon va fermer xo\'jaliklarining moddiy-texnik bazasini mustahkamlash. Rais
                        Doskalar</p>',
            'bank_mission_en' => '<p>Dear shareholders! Dear ladies and gentlemen!</p>
                    <p>The past year was marked by accelerated growth
                        economy of our country, a lot of work has been done to further increase liquidity and
                        strengthening the stability of banks. </p>
                    <p>Positive changes have been achieved in credit and investment
                        banking activities. The total volume of loans directed to the real sector is growing year by year
                        economy. Decrees and resolutions played an important role in these positive changes.
                        President, which served as policy documents for development.</p>
                    <p>We are proud and confident
                        we can say that in the growth of the country\'s economic indicators there is a share of the equity
                        commercial bank "Agrobank", and this is not an exaggeration. </p>
                    <p>During the past year 2017
                        Agrobank, carried out consistent measures to fulfill the established tasks arising from
                        priority areas of the country\'s economic development, which are aimed at further
                        increase in capitalization and investment activity of the bank, further expansion of the volume
                        lending aimed at modernizing production, creating new jobs. </p>
                    <p>As a result of the measures taken to financially strengthen the bank, the authorized capital of the bank reached
                        volume
                        1335.6 billion soums. </p>
                    <p>The resource base of the bank in the reporting year amounted to 4392.7 billion soums,
                        assets amounted to 4981.5 billion soums. In the reporting year, the bank continued its policy of increasing
                        attraction of temporarily free funds of the population in deposits, as a result of which
                        On January 1, 2018, the volume of deposits amounted to 1659.5 billion soums. In addition, posted
                        certificates of deposit in the amount of 135.2 billion soums. When funding and
                        lending to the agriculture of the republic, the main attention was paid to financial support
                        agricultural complex, increasing industrial production, harvesting and processing
                        agricultural products, farm financing. Mutually beneficial
                        relations with international rating agencies and international financial institutions. AT
                        the current financial year, the bank will continue to implement its strategic business plan,
                        aimed at further ensuring its stability and efficiency, in
                        accordance with international norms and standards. </p>
                    <p>Will expand the types and scope of rendered
                        services, provide comprehensive financial support to enterprises in the real sector of the economy,
                        develop small business and private entrepreneurship, make every effort and opportunity
                        to strengthen the material and technical base of dekhkan and farm enterprises. Chairman
                        Boards</p>'

        ]);

    }
}
