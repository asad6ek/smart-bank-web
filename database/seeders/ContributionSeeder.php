<?php

namespace Database\Seeders;

use App\Models\Contribution;
use Illuminate\Database\Seeder;

class ContributionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'label_uz' => 'Depozit muddati',
                'label_ru' => 'Срок вклада',
                'label_en' => 'Deposit term',
                'value_ru' => '23%',
                'value_en' => '23%',
                'value_uz' => '23%',
            ],
            [
                'label_uz' => 'Yillik foiz stavkasi oflayn',
                'label_ru' => 'Годовая процентная ставка офлайн',
                'label_en' => 'Annual interest rate offline',
                'value_ru' => '22%',
                'value_en' => '22%',
                'value_uz' => '22%',
            ],
            [
                'label_uz' => 'Depozit muddati',
                'label_ru' => 'Срок вклада',
                'label_en' => 'Deposit term',
                'value_uz' => '13 oy',
                'value_ru' => '13 месяцев',
                'value_en' => '13 months',

            ],
            [
                'label_uz' => 'Minimal miqdor',
                'label_ru' => 'Минимальная сумма',
                'label_en' => 'Minimum amount',
                'value_uz' => '500 000 so`m',
                'value_ru' => '500 000 сумов',
                'value_en' => '500 000 uzs',

            ],
            [
                'value_uz' => '“Agrobank Mobile” mobil ilovasi orqali qo‘yilgan omonatlar bo‘yicha hisoblangan foizlar avtomatik ravishda p/c ga o‘tkaziladi;',
                'value_ru' => 'Начисленные проценты по депозитам, оформленным через мобильное приложение «Agrobank Mobile» автоматически переводиться на п/к;',
                'value_en' => 'Accrued interest on deposits made through the mobile application "Agrobank Mobile" is automatically transferred to p / c;',
                'is_checkbox' => true
            ],
            [
                'value_uz' => 'Agar omonat muddati o‘tganidan keyin qaytarib olinmasa yoki boshqa omonat turi bo‘yicha qayta ro‘yxatdan o‘tkazilmagan bo‘lsa, omonat bo‘yicha foizlar “talab qilib” qo‘yilgan omonat stavkasi bo‘yicha to‘lanadi.',
                'value_ru' => 'Если вклад не снимается по истечении срока или не перео формляется на другой вид вклада, проценты по вкладу выплачиваются по ставке вклада «до востребования»',
                'value_en' => 'If the deposit is not withdrawn after the expiration of the term or is not re-registered for another type of deposit, the interest on the deposit is paid at the rate of the deposit "on demand"',
                'is_checkbox' => true
            ],
            [
                'value_uz' => '“Agrobank Mobile” mobil ilovasi orqali qo‘yilgan omonatlar bo‘yicha hisoblangan foizlar avtomatik ravishda p/c ga o‘tkaziladi;',
                'value_ru' => 'Начисленные проценты по депозитам, оформленным через мобильное приложение «Agrobank Mobile» автоматически переводиться на п/к;',
                'value_en' => 'Accrued interest on deposits made through the mobile application "Agrobank Mobile" is automatically transferred to p / c;',
                'is_checkbox' => true,
                'is_about_deposit' => true,
            ],
            [
                'value_uz' => 'Agar omonat muddati o‘tganidan keyin qaytarib olinmasa yoki boshqa omonat turi bo‘yicha qayta ro‘yxatdan o‘tkazilmagan bo‘lsa, omonat bo‘yicha foizlar “talab qilib” qo‘yilgan omonat stavkasi bo‘yicha to‘lanadi.',
                'value_ru' => 'Если вклад не снимается по истечении срока или не перео формляется на другой вид вклада, проценты по вкладу выплачиваются по ставке вклада «до востребования»',
                'value_en' => 'If the deposit is not withdrawn after the expiration of the term or is not re-registered for another type of deposit, the interest on the deposit is paid at the rate of the deposit "on demand"',
                'is_checkbox' => true,
                'is_about_deposit' => true,
            ],
        ];
        foreach ($data as $val)
            Contribution::query()->firstOrCreate([
                'value_uz' => $val['value_uz'],
                'is_about_deposit' => isset($val['is_about_deposit'])
            ], $val);
    }
}
