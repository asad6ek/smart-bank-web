<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(ContributionSeeder::class);
        $this->call(CardsSeeder::class);
        $this->call(LegalUsersSeeder::class);
        $this->call(AboutOfBankSeeder::class);
        $this->call(ManagementSeeder::class);
        $this->call(VacanciesSeeder::class);
        $this->call(SocialNetworksSeeder::class);
    }
}
