<?php

namespace Database\Seeders;

use App\Models\LegalUser;
use Illuminate\Database\Seeder;

class LegalUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LegalUser::query()->firstOrCreate([
            'label_1_uz' => 'BankZamin bank kartalarini taqdim etish va ulardan foydalanish qoidalari'
        ], [
            'label_1_uz' => 'BankZamin bank kartalarini taqdim etish va ulardan foydalanish qoidalari',
            'label_1_ru' => 'Правила предоставления и использования банковских карт BankZamin',
            'label_1_en' => 'Rules for the provision and use of BankZamin bank cards',
            'label_2_uz' => 'BankZamin bank kartalarini taqdim etish va ulardan foydalanish qoidalari',
            'label_2_ru' => 'Правила предоставления и использования банковских карт BankZamin',
            'label_2_en' => 'Rules for the provision and use of BankZamin bank cards',
            'label_3_uz' => 'BankZamin bank kartalarini taqdim etish va ulardan foydalanish qoidalari',
            'label_3_ru' => 'Правила предоставления и использования банковских карт BankZamin',
            'label_3_en' => 'Rules for the provision and use of BankZamin bank cards',
            'description_1_uz' => 'Kredit shartnomalari bo\'yicha huquqlarni (da\'volarni) paketli boshqa shaxsga o\'tkazish maqsadida asosiy faoliyat turi sifatida muddati o\'tgan qarzlarni qaytarish bo\'yicha faoliyatni amalga oshiruvchi tashkilotlarni tanlash to\'g\'risidagi ma\'lumotlar.',
            'description_1_ru' => 'Информация об отборе организаций, осуществляющих деятельность по возврату просроченной задолженности в качестве основного вида деятельности для целей пакетной уступки прав (требований) по кредитным договорам',
            'description_1_en' => 'Information on the selection of organizations that carry out activities for the return of overdue debts as the main activity for the purposes of a package assignment of rights (claims) under loan agreements',
            'description_2_uz' => 'Jismoniy shaxslarga kredit berish maqsadida mol-mulkni baholash bo‘yicha tavsiya etilgan baholash tashkilotlari ro‘yxatiga kiritish uchun baholash tashkilotlarini tanlash to‘g‘risidagi ma’lumotlar',
            'description_2_ru' => 'Информация об отборе Оценочных организаций на предмет включения в перечень рекомендованных оценочных организаций для оценки имущества в целях кредитования физических лиц',
            'description_2_en' => 'Information on the selection of Appraisal Organizations for inclusion in the list of recommended appraisal organizations for property valuation for the purpose of lending to individuals',
        ]);

    }
}
