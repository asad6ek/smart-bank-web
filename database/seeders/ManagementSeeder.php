<?php

namespace Database\Seeders;

use App\Models\Managements;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
            ], [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
            ], [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
                'is_council' => 1
            ], [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
                'is_council' => 1
            ], [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
                'is_council' => 1
            ], [
                'full_name' => 'Фамилия Имя Отчество',
                'short_bio_ru' => 'Заместитель Министра финансов Республики Узбекистан, Председатель Cовета',
                'short_bio_uz' => 'O‘zbekiston Respublikasi moliya vazirining o‘rinbosari, Kengash raisi',
                'short_bio_en' => 'Deputy Minister of Finance of the Republic of Uzbekistan, Chairman of the Council',
                'image_path_1' => 'https://via.placeholder.com/221x304.jpg',
                'image_path_2' => ' https://via.placeholder.com/442x608.webp',
                'is_council' => 1
            ],
        ];
        foreach ($data as $val)
            Managements::query()->create($val);
    }
}
