<?php

namespace Database\Seeders;

use App\Models\SocialNetwork;
use Illuminate\Database\Seeder;

class SocialNetworksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Telegram',
                'link' => '#',
                'status' => 1,
            ],
            [
                'name' => 'Facebook',
                'link' => '#',
                'status' => 1,
            ],
            [
                'name' => 'Instagram',
                'link' => '#',
                'status' => 1,
            ],
            [
                'name' => 'Twitter',
                'link' => '#',
                'status' => 1,
            ],
            [
                'name' => 'App Store',
                'link' => '#',
                'status' => 1,
                'is_store' => 1
            ],
            [
                'name' => 'Google Store',
                'link' => '#',
                'status' => 1,
                'is_store' => 1
            ],
        ];
        foreach ($data as $value)
            SocialNetwork::query()->firstOrCreate(['name' => $value['name']], $value);
    }
}
