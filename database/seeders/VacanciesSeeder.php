<?php

namespace Database\Seeders;

use App\Constant\VacancyType;
use Illuminate\Database\Seeder;

class VacanciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title_tu' => 'Агент по продажам',
                'title_en' => 'Sales agent',
                'title_uz' => 'Savdo agenti',
                'from' => 3500000,
                'type_of_work' => VacancyType::FULL_TIME,
                'status' => 1,
            ],
            [
                'title_tu' => 'Оператор call-центра',
                'title_en' => 'Call center operator',
                'title_uz' => 'Qo\'ng\'iroqlar markazi operatori',
                'from' => 3500000,
                'type_of_work' => VacancyType::FULL_TIME,
                'status' => 1,
            ],
        ];


    }
}
