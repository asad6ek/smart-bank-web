<?php

use Illuminate\Support\Facades\Storage;

/**
 * @param string|null $path
 * @return string
 */
function getFileSize(string $path = null): string
{
    $bytes = $path ? Storage::size('public/' . $path) : 0;

    if ($bytes >= 1073741824) {
        return number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        return number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        return number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes >= 1) {
        return $bytes . ' bytes';
    }
    return '0 bytes';
}
