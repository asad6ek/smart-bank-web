@extends('frontend.layouts.app')
@section('contend')
    <section class="general__section wrapper">
        <div class="breadcrumb__area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="breadcrumb__area-title">@lang('app.bank_inf')</h1>
                        <p class="breadcrumb__area-text mb-0">@lang('app.mission_agrobank')</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container">
                <h2 class="fw-bold mb-5">@lang('app.main_tasks'):</h2>

                <div class="mb-6 mb-lg-8">
                    {!! $model->{'main_tasks_' . app()->getLocale()} !!}
                </div>
                @if($model->file_path )
                    <div class="download__file d-flex align-items-center mb-8 mb-lg-12">
                        <div class="me-3">
                            <img src="{{asset("assets-frontend/images/icons/pdf.svg")}}" width="32" height="28"
                                 alt="icon">
                        </div>
                        <div>
                            <div class="title mb-2">@lang('app.bank_license')</div>
                            <div class="size mb-2">PDF, {{ getFileSize($model->file_path) }}</div>
                            <a href="/storage/{{$model->file_path }}"
                               class="btn btn-secondary rounded-btn py-2 px-3">@lang('app.more')</a>
                        </div>
                    </div>
                @endif

                <div class="border-top border-bottom py-5 py-lg-6 mb-8 mb-lg-12">
                    <h3 class="h4 fw-bold mb-4">@lang('app.mission_bank')</h3>
                    {!! $model->{'short_bank_mission_' . app()->getLocale()} !!}
                </div>

                <div class="border-top border-bottom py-5 py-lg-6">
                    <h3 class="h4 fw-bold mb-3">@lang('app.mission_bank')</h3>
                    <h4 class="h5 fw-bold mb-4">@lang('app.message_chairman')</h4>
                    <div class="border-primary border-start border-2 ps-3">
                        {!! $model->{'bank_mission_' . app()->getLocale()} !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
