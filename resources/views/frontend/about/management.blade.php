@extends('frontend.layouts.app')
@section('contend')
    <section class="general__section wrapper mb-1">
        <div class="breadcrumb__area pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="breadcrumb__area-title mb-12">@lang('app.management')</h1>

                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link bg-transparent active"
                                        id="council-tab"
                                        data-bs-toggle="pill"
                                        data-bs-target="#council"
                                        type="button"
                                        role="tab"
                                        aria-controls="council"
                                        aria-selected="true">
                                    @lang('app.bank_board')
                                </button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button class="nav-link bg-transparent"
                                        id="management-tab"
                                        data-bs-toggle="pill"
                                        data-bs-target="#management"
                                        type="button"
                                        role="tab"
                                        aria-controls="management"
                                        aria-selected="false">
                                    @lang('app.bank_council')
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="council" role="tabpanel" aria-labelledby="council-tab"
                         tabindex="0">
                        <div class="row gy-12">
                            @foreach($managements as $management)
                                @if(!$management->is_council)
                                    <div class="col-12">
                                        <div class="staff__card d-flex flex-column flex-lg-row">
                                            <div class="staff__card-img text-center">
                                                <picture>
                                                    <source
                                                        data-srcset="{{asset('storage/'.$management->image_path_1)}} 1x, {{asset('storage/'.$management->image_path_2)}} 2x"
                                                        type="image/webp"/>
                                                    <img class="lazyload img-fluid section__img "
                                                         style="background-repeat: no-repeat; width: 221px; height: 304px"
                                                         loading="lazy"
                                                         data-src="{{asset('storage/'.$management->image_path_1)}}"
                                                         alt="image description"/>
                                                </picture>
                                            </div>
                                            <div class="staff__card-content">
                                                <h2 class="title mb-2 mb-lg-3">{{$management->full_name}}</h2>
                                                <p class="description">{{$management->{'short_bio_'. app()->getLocale()} }}</p>
                                                <a class="btn btn-primary rounded-5 py-3 px-4"
                                                   data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                    @lang('app.more')
                                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                                        <use
                                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="management" role="tabpanel" aria-labelledby="management-tab"
                         tabindex="0">
                        <div class="row gy-12">
                            @foreach($managements as $management)
                                @if(!$management->council)
                                    <div class="col-12">
                                        <div class="staff__card d-flex flex-column flex-lg-row">
                                            <div class="staff__card-img text-center">
                                                <picture>
                                                    <source
                                                        data-srcset="{{asset('storage/'.$management->image_path_1)}} 1x, {{asset('storage/'.$management->image_path_2)}} 2x"
                                                        type="image/webp"/>
                                                    <img class="lazyload img-fluid section__img "
                                                         style="background-repeat: no-repeat; width: 221px; height: 304px"
                                                         loading="lazy"
                                                         data-src="{{asset('storage/'.$management->image_path_1)}}"
                                                         alt="image description"/>
                                                </picture>
                                            </div>
                                            <div class="staff__card-content">
                                                <h2 class="title mb-2 mb-lg-3">{{$management->full_name}}</h2>
                                                <p class="description">{{$management->{'short_bio_'. app()->getLocale()} }}</p>
                                                <a class="btn btn-primary rounded-5 py-3 px-4"
                                                   data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                    @lang('app.more')
                                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                                        <use
                                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
