@extends('frontend.layouts.app')
@section('contend')
    <section class="general__section wrapper">
        <div class="breadcrumb__area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="breadcrumb__area-title">@lang('app.for_partner')</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container">
                <div class="mb-7 mb-lg-12">
                    <h2 class="fw-bold mb-5">
                        {{ $model-> {'description_1_'.app()->getLocale()} }}
                    </h2>

                    <div class="download__file d-flex align-items-center mb-6">
                        <div class="me-3">
                            <img src="{{asset("assets-frontend/images/icons/pdf.svg")}}" width="32" height="28"
                                 alt="icon">
                        </div>
                        <div>
                            <div class="title mb-2">{{$model->{'label_1_'.app()->getLocale()} }}
                            </div>
                            <div class="size mb-2">PDF, {{ getFileSize($model->file_path_1) }}</div>
                            <a href="/storage/{{$model->file_path_1 }}" class="btn btn-secondary rounded-btn py-2 px-3">@lang('app.more')</a>
                        </div>
                    </div>

                    <div class="download__file d-flex align-items-center mb-6">
                        <div class="me-3">
                            <img src="{{asset("assets-frontend/images/icons/pdf.svg")}}" width="32" height="28"
                                 alt="icon">
                        </div>
                        <div>
                            <div class="title mb-2">{{$model->{'label_2_'.app()->getLocale()} }}
                            </div>
                            <div class="size mb-2">PDF, {{ getFileSize($model->file_path_2) }}</div>
                            <a href="/storage/{{$model->file_path_2 }}" class="btn btn-secondary rounded-btn py-2 px-3">@lang('app.more')</a>
                        </div>
                    </div>
                </div>

                <div>
                    <h3 class="h2 fw-bold mb-5">
                        {{ $model-> {'description_2_'.app()->getLocale()} }}
                    </h3>

                    <div class="download__file d-flex align-items-center">
                        <div class="me-3">
                            <img src="{{asset("assets-frontend/images/icons/pdf.svg")}}" width="32" height="28"
                                 alt="icon">
                        </div>
                        <div>
                            <div class="title mb-2">{{$model->{'label_3_'.app()->getLocale()} }}
                            </div>
                            <div class="size mb-2">PDF, {{ getFileSize($model->file_path_3) }}</div>
                            <a href="/storage/{{$model->file_path_3 }}" class="btn btn-secondary rounded-btn py-2 px-3">@lang('app.more')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
