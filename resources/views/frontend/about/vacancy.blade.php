@extends('frontend.layouts.app')
@section('contend')
    <section class="general__section wrapper">
        <div class="breadcrumb__area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="breadcrumb__area-title">@lang('app.jobs')</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 mx-auto">
                        <div>
                            <div class="row gy-6">
                                <div class="col-12">
                                    <h2 class="h1 fw-bold mb-3">@lang('app.vacancy_title')</h2>
                                    <span class="fw-bold text-gray">
                                        {{__('app.jobs_count',['attribute'=>$vacancies->count()])}}
                                    </span>
                                </div>
                                @foreach($vacancies as $vacancy)
                                    <div class="col-12">
                                        <div
                                            class="vacancy__item row gy-3 justify-content-between align-items-center border-top border-bottom py-4">
                                            <div class="col-auto">
                                                <h3 class="vacancy__item-title mb-1">{{$vacancy->{'title_'. app()->getLocale()} }}</h3>
                                                <span
                                                    class="vacancy__item-salary"> @lang('app.from') {{ number_format($vacancy->from, thousands_separator: ' ') }} @lang('app.sum') / {{\App\Constant\VacancyType::getNameByKey($vacancy->type_of_work)}}
                                                </span>
                                            </div>

                                            <div class="col-sm-4 text-end">
                                                <a href="#!"
                                                   class="btn btn-secondary rounded-btn py-2 px-3">@lang('app.more')</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
