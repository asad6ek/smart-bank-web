@extends('frontend.layouts.app')
@section('contend')
    <section class="blog wrapper">
        <div class="container">
            <div class="row gx-0">
                <div class="col-xl-8">
                    <div class="bog__content border-end-xl">
                        <div class="bog__content-header border-bottom d-flex justify-content-between">
                            <h1 class="blog__title">@lang('app.news')</h1>
                            <div class="blog__dropdown dropdown dropdown-menu-end">
                                <button class="btn btn-secondary px-3 rounded-btn dropdown-toggle"
                                        type="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                    2022
                                </button>
                                <ul class="dropdown-menu">
                                </ul>
                            </div>
                        </div>
                        <div class="bog__content-body pb-10">
                           @if($latest)
                            <article class="blog_hero" style="--hero-img: url({{ '/storage/'.$latest->image_path_1 }})">
                                <div class="blog__item-time text-white">
                                    <svg width="13" height="18" class="me-1">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#calendar"></use>
                                    </svg>
                                    {{$latest->created_at->format('d.m.Y')}}
                                </div>

                                <div class="category">@lang('app.news')</div>
                                <h2 class="title text__two-line">
                                    <a href="{{route('blog.view',['blog' => $latest->id])}}" class="border-link">
                                        {{$latest->{'title_' . app()->getLocale()} }}
                                    </a>
                                </h2>
                            </article>
                            @endif
                            <div class="row mb-5">
                                @foreach($blogs as $blog)
                                    <div class="col-12 gy-6">
                                        <article class="blog__item d-flex flex-column flex-sm-row">
                                            <a href="{{route('blog.view',['blog' => $blog->id])}}"
                                               class="ripple-link blog__item-img mb-5 mb-sm-0 me-sm-5 flex-shrink-0">
                                                <picture>
                                                    <source
                                                        data-srcset="{{ asset('storage/'.$blog->image_path_1) }} 1x, {{ asset('storage/'.$blog->image_path_2) }} 2x"
                                                        type="image/png"/>
                                                    <img class="lazyload rounded-4 img-fluid"
                                                         loading="lazy"
                                                         width="294"
                                                         height="150"
                                                         data-src="{{asset("assets-frontend/images/news/news.png")}}"
                                                         alt="image description"/>
                                                </picture>
                                            </a>
                                            <div class="blog__item-info">
                                                <div class="blog__item-category mb-2">@lang('app.news')</div>
                                                <h3 class="blog__item-title text__two-line">
                                                    <a href="{{route('blog.view',['blog' => $blog->id])}}"
                                                       class="border-link">
                                                        {{$blog->{'title_' . app()->getLocale()} }}
                                                    </a></h3>
                                                <p class="blog__item-text text__two-line mb-3">
                                                    {!! $blog->{'short_description_' . app()->getLocale()} !!}
                                                </p>
                                                <div class="blog__item-time">
                                                    <svg width="13" height="18" class="me-1">
                                                        <use
                                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#calendar"></use>
                                                    </svg>
                                                    {{$blog->created_at->format('d.m.Y')}}
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach

                            </div>
                            {{ $blogs->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>

                <div class="col-xl-4">
                    <aside class="blog__sidebar">
                        <div class="subscription mb-6">
                            <div class="mb-5">
                                <p class="mb-1 title">@lang('app.subscription')</p>
                                <span class="text">@lang('app.news_updates')</span>
                            </div>

                            <div class="form-floating form-floating-sm mb-3">
                                <input type="email" class="form-control bg-white" id="floatingInput"
                                       placeholder="BankZamin@gmail.com">
                                <label for="floatingInput">E-mail</label>
                            </div>

                            <button class="btn btn-primary py-3 px-4 rounded-5">@lang('app.subscribe')</button>
                        </div>

                        <div>
                            <div class="anchor__slider swiper">
                                <h4 class="mb-3">@lang('app.useful_links')</h4>
                                <div class="swiper-wrapper">
                                    @foreach($blogs as $blog)
                                        <div class="swiper-slide">
                                            <ul class="list-unstyled mb-0">
                                                <li><a class="text-decoration-underline link-dark text__two-line"
                                                       href="{{route('blog.view',['blog' => $blog->id])}}">
                                                        {{$latest->{'title_' . app()->getLocale()} }}</a></li>
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="d-flex mt-4">
                                    <div class="swiper-button-prev position-static rounded-circle me-3"></div>
                                    <div class="swiper-button-next position-static rounded-circle"></div>
                                </div>
                            </div>
                        </div>

                    </aside>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.layouts.breadcrumbs',['breadcrumbs'=>[
    [
        'route' => route('blog.index'),
        'name' =>'Новости'
     ]]])
@endsection
