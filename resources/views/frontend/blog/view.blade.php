@extends('frontend.layouts.app')
@section('contend')
    <section class="blog wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="blog__view">
                        <div class="blog__view-content">
                            <h1 class="mb-5 blog__view-title">
                                {{$model->{'title_' . app()->getLocale()} }}
                            </h1>

                            <div class="d-flex mb-5">
                                <div class="blog__item-time me-5">
                                    <svg width="13" height="18" class="me-1">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#calendar"></use>
                                    </svg>
                                    {{$model->created_at->format('d.m.Y')}}
                                </div>
                                <div class="blog__item-time">
                                    <svg width="13" height="18" class="me-1">
                                        <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#eye"></use>
                                    </svg>
                                    {{$model->review_count }}
                                </div>
                            </div>

                            {!! $model->{'short_description_' . app()->getLocale()} !!}

                            <img data-src="{{ asset('storage/'.$model->image_path_1) }}"
                                 data-srcset="{{ asset('storage/'.$model->image_path_1) }} 1x, {{ asset('storage/'.$model->image_path_2) }} 2x"
                                 loading="lazy"
                                 class="lazyload img-fluid"
                                 width="764"
                                 height="468"
                                 alt="image description">

                            {!! $model->{'description_' . app()->getLocale()} !!}

                        </div>

                        <div class="share__post">
                            <p class="mb-3">@lang('app.share_news'):</p>
                            <div class="row gx-3">
                                <div class="col-auto">
                                    <noindex>
                                        <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                           target="_blank">
                                            <svg width="18" height="18">
                                                <use
                                                    xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#tg"></use>
                                            </svg>
                                        </a>
                                    </noindex>
                                </div>
                                <div class="col-auto">
                                    <noindex>
                                        <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                           target="_blank">
                                            <svg width="18" height="18">
                                                <use
                                                    xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#fb"></use>
                                            </svg>
                                        </a>
                                    </noindex>
                                </div>
                                <div class="col-auto">
                                    <noindex>
                                        <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                           target="_blank">
                                            <svg width="18" height="18">
                                                <use
                                                    xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#ig"></use>
                                            </svg>
                                        </a>
                                    </noindex>
                                </div>
                                <div class="col-auto">
                                    <noindex>
                                        <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                           target="_blank">
                                            <svg width="18" height="18">
                                                <use
                                                    xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#tw"></use>
                                            </svg>
                                        </a>
                                    </noindex>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-11">
                <div class="d-flex justify-content-between align-items-center mb-5">
                    <h3 class="h4 me-3 fw-bold">@lang('app.news_events')</h3>
                    <a href="{{route('blog.index')}}"
                       class="text-primary text-decoration-underline fw-bold d-none d-lg-inline-block">
                        @lang('app.see_all_news')</a>
                </div>
                <div class="blog__slider swiper">
                    <div class="swiper-wrapper">
                        @foreach($blogs as $blog)
                            <div class="swiper-slide">
                                <a href="{{route('blog.view',['blog' => $blog->id])}}">
                                    <div class="news__item-header">
                                        <div class="news__item-category">Новости</div>
                                        <h4 class="blog__item-title text__three-line mb-2">
                                            {{$blog->{'title_' . app()->getLocale()} }}
                                        </h4>
                                    </div>
                                    <div class="news__item-footer">

                                        <div class="news__item-time">
                                            <svg width="13" height="18" class="me-1">
                                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#calendar"></use>
                                            </svg>
                                            {{$blog->created_at->format('d.m.Y')}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
