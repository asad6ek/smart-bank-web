@extends('frontend.layouts.app')
@section('contend')
    <section class="wrapper">
        <div class="card__header">
            <div class="container">
                <div class="position-relative">
                    <div class="row gy-6">
                        <div class="col-lg-7">
                            <h1 class="card__view-title mb-3">@lang('app.smart_card')</h1>
                            <p class="card__view-text mb-5">
                                {{$card->{'value_'.app()->getLocale()} }}
                            </p>
                            <a href="#card-form" class="btn btn-white rounded-5 py-3 px-4 fw-semibold">
                                @lang('app.make_request')
                            </a>
                        </div>
                        <div class="col-lg-5">
                            <div class="card__header-img position-relative">
                                <img data-src="{{asset("assets-frontend/images/cards/card-view.png")}}"
                                     data-srcset="{{asset("assets-frontend/images/cards/card-view.png")}} 1x, {{asset("assets-frontend/images/cards/card-view@2x.png")}} 2x"
                                     loading="lazy"
                                     class="lazyload img-fluid"
                                     alt="image description">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card__advantages">
            <div class="container">
                <div class="row gy-5">
                    @foreach($models as $model)
                        @if($model->type == \App\Models\Card::IS_ADVANTAGE)
                            <div class="col-sm-6 col-lg-4">
                                <div class="card__view-advantage h-100">
                                    <h6 class="advantage__label mb-9">{{$model->{'label_'.app()->getLocale()} }}</h6>

                                    <div>
                                        <p class="advantage__value mb-1">{{$model->{'value_'.app()->getLocale()} }}</p>
                                        <span class="advantage__text">
                                        {{$model->{'description_'.app()->getLocale()} }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card__info">
            <div class="container">
                <h2 class="card__info-title mb-5 mb-lg-6">@lang('app.map_information')</h2>

                <ul class="list-unstyled card__info-list border rounded-5 mb-6 mb-lg-8">
                    @foreach($models as $model)
                        @if($model->type == \App\Models\Card::IS_CHECKBOX)
                            <li class="d-flex align-items-center">
                                <div
                                    class="icon rounded-circle flex-shrink-0 me-3 d-flex align-items-center justify-content-center">
                                    <svg width="9" height="7">
                                        <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#check">
                                        </use>
                                    </svg>
                                </div>
                                <p class="mb-0">{{$model->{'value_'.app()->getLocale()} }}</p>
                            </li>
                        @endif
                    @endforeach
                </ul>

                <h3 class="mb-5 card__info--title">@lang('app.together_bankzamin_get')</h3>

                <ul class="card__info-list-disc mb-6 mb-lg-12">
                    @foreach($models as $model)
                        @if($model->type == \App\Models\Card::IS_INF_LIST)
                            <li>
                                <p class="mb-0">{{$model->{'value_'.app()->getLocale()} }}</p>
                            </li>
                        @endif
                    @endforeach
                </ul>

                <div class="card__request border rounded-5" id="card-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <img data-src="{{asset("assets-frontend/images/cards/primary-card.png")}}"
                                 data-srcset="{{asset("assets-frontend/images/cards/primary-card.png")}} 1x, {{asset("assets-frontend/images/cards/primary-card@2x.png")}} 2x"
                                 loading="lazy"
                                 class="lazyload mb-3 mb-lg-5"
                                 width="182"
                                 height="117"
                                 alt="image description">

                            <h5 class="card__request-title mb-3 mb-lg-0">@lang('app.leave_request_smartcard')</h5>
                        </div>
                        <div class="col-lg-6">
                            <form action="{{route('site.send-application')}}" class="row gy-3"
                                  enctype="multipart/form-data" method="post">
                                @csrf
                                @method('POST')
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="floatingInput" name="full_name" placeholder="@lang('app.full_name')">
                                        <label for="floatingInput">@lang('app.full_name')</label>
                                    </div>
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" id="floatingInput2"
                                               placeholder="BankZamin@gmail.com" name="email">
                                        <label for="floatingInput2">@lang('app.mail')</label>
                                    </div>
                                    @error('email')
                                    <div class="alert text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-12">
                                    <div class="form-floating @error('phone_number') is-invalid @enderror">
                                        <input type="text" class="form-control " id="floatingInput3"
                                               placeholder="+998" name="phone_number">
                                        <label for="floatingInput3">@lang('app.tel')</label>

                                    </div>
                                    @error('phone_number')
                                    <div class="alert text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-12">
                                    <p class="fw-bold">@lang('app.attach_scan')</p>
                                    <div class="row gy-3 align-items-center">
                                        <div class="col-6">
                                            <div class="input-file-container position-relative">
                                                <input
                                                    class="input-file position-absolute top-0 end-0 bottom-0 start-0 opacity-0 p-0"
                                                    id="my-file" type="file" name="file_path">
                                                <label tabindex="0" for="my-file" class="input-file-trigger">
                                                    @lang('app.attach_file')
                                                </label>
                                                @error('file_path')
                                                <div class="alert text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <p class="fw-medium mb-0">@lang('app.max_file')</p>
                                        </div>
                                    </div>

                                    <p class="file-return" data-title="@lang('app.selected_file')"></p>
                                </div>

                                <div class="col-12">
                                    <button class="btn primary-gradient rounded-4 w-100 border-0 p-4 text-white">
                                        @lang('app.continue_checkout')
                                    </button>
                                </div>

                                <div class="col-12">
                                    <div class="border p-4 rounded-4">
                                        <img src="{{asset("assets-frontend/images/icons/guarantee.svg")}}"
                                             alt="guarantee" class="me-3" width="16" height="16">
                                        @lang('app.we_guarantee_data')
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
