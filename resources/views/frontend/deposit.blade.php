@extends('frontend.layouts.app')
@section('contend')
    <section class="general__section wrapper">
        <div class="breadcrumb__area bg-green position-relative pt-lg-0 pb-0">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h1 class="breadcrumb__area-title">@lang('app.smart_contribution')</h1>
                        <p class="breadcrumb__area-text mb-0">@lang('app.connect_subscription')</p>
                    </div>

                    <div class="col-lg-5">
                        <div class="breadcrumb__area-img text-center">
                            <img data-src="{{asset("assets-frontend/images/banner/banner3.png")}}"
                                 data-srcset="{{asset("assets-frontend/images/banner/banner3.png")}} 1x, {{asset("assets-frontend/images/banner/banner3@2x.png")}} 2x"
                                 loading="lazy"
                                 class="lazyload img-fluid"
                                 alt="image description">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container">
                <div class="mb-6 mb-lg-12">
                    <h2 class="card__info-title mb-5 mb-lg-6">@lang('app.annual_rate')</h2>

                    <div class="row g-4 g-lg-5">
                        @foreach($models as $model)
                            @if(!$model->is_checkbox)
                                <div class="col-lg-4">
                                    <div class="card__view-advantage h-100">
                                        <h6 class="advantage__label mb-9">{{$model->{'label_'.app()->getLocale()} }}</h6>

                                        <div>
                                            <p class="advantage__value mb-1">{{$model->{'value_'.app()->getLocale()} }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="col-lg-8">
                            <div class="download__file d-flex align-items-center border px-5 py-6 rounded-5">
                                <div class="me-3">
                                    <img src="{{asset("assets-frontend/images/icons/pdf.svg")}}" width="32" height="28"
                                         alt="icon">
                                </div>
                                <div>
                                    <div class="title mb-2">@lang('app.bank_license')</div>
                                    <div class="size mb-2">PDF, {{getFileSize($about_of_bank->file_path)}}</div>
                                    <a href="{{$about_of_bank->file_path ?('/storage/'.$about_of_bank->file_path):'#'}}"
                                       class="btn btn-secondary rounded-btn py-2 px-3">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="list-unstyled card__info-list border rounded-5 mb-6 mb-lg-9">
                    @foreach($models as $model)
                        @if($model->is_checkbox and !$model->is_about_deposit)
                            <li class="d-flex align-items-center">
                                <div
                                    class="icon rounded-circle flex-shrink-0 me-3 d-flex align-items-center justify-content-center">
                                    <svg width="9" height="7">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#check"></use>
                                    </svg>
                                </div>

                                <p class="mb-0">{{$model->{'value_'.app()->getLocale()} }}</p>
                            </li>
                        @endif
                    @endforeach
                </ul>

                <div class="section pb-0 right mb-6 mb-lg-12">
                    <div class="section__content position-relative">
                        <div class="row gy-4">
                            <div class="col-lg-8">
                                <div>
                                    <h4 class="section__title mb-5 mb-lg-8">@lang('app.opening_deposit')</h4>

                                    <a href="#!" class="btn btn-primary rounded-5 py-3 px-4">
                                        Подробнее
                                        <svg width="10" height="10" class="ms-1 align-baseline">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="text-center">
                                    <img data-src="{{asset("assets-frontend/images/banner/banner2.png")}}"
                                         data-srcset="{{asset("assets-frontend/images/banner/banner2.png")}} 1x, {{asset("assets-frontend/images/banner/banner2@2x.png")}} 2x"
                                         loading="lazy"
                                         class="lazyload img-fluid section__img"
                                         alt="image description">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="border py-5 pt-xl-11 pb-xl-8 px-4 px-xl-8 rounded-5">
                    <div class="row">
                        <div class="col-lg-3">
                            <h5 class="card__request-title mb-3 mb-lg-0">@lang('app.about_deposit')</h5>
                        </div>
                        <div class="col-lg-9">
                            <form class="row gy-5">
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="floatingInput"
                                               placeholder="50 000 000 @lang('app.sum')">
                                        <label for="floatingInput">@lang('app.deposit_amount')</label>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="row align-items-sm-center gy-5">
                                        <div class="col-sm-6">
                                            <div>
                                                <span class="fw-medium">@lang('app.monthly_income')</span>
                                                <span class="mb-0 fw-bold h3 d-block">904 200 @lang('app.sum')</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn primary-gradient rounded-4 w-100 border-0 p-4 text-white waves-effect waves-button waves-light">
                                                @lang('app.calculate')
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <ul class="list-unstyled card__info-list border rounded-5 mb-0">
                                        @foreach($models as $model)
                                            @if($model->is_checkbox and $model->is_about_deposit)
                                                <li class="d-flex align-items-center">
                                                    <div
                                                        class="icon rounded-circle flex-shrink-0 me-3 d-flex align-items-center justify-content-center">
                                                        <svg width="9" height="7">
                                                            <use
                                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#check"></use>
                                                        </svg>
                                                    </div>

                                                    <p class="mb-0">{{$model->{'value_'.app()->getLocale()} }}</p>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
