@extends('frontend.layouts.app')
@section('contend')
    <section class="hero position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-6">
                    <div class="hero__content">
                        <h1 class="hero__title mb-3">@lang('app.manage_finances')</h1>
                        <p class="hero__text mb-3 mb-sm-6">@lang('app.scan_send_link')</p>

                        <div class="d-flex">
                            <img src="{{asset("assets-frontend/images/qr-code.svg")}}" alt="qr-code"
                                 class="qr-code me-3" height="140"
                                 width="140">

                            <div class="w-100">
                                <div class="hero__searchbox d-flex mb-3">
                                    <input type="text" class="form-control box-s border-0" placeholder="@lang('app.enter_number')">
                                    <button class="btn btn-warning flex-shrink-0 rounded-btn py-3 px-3 px-sm-4" type="button">
                                        <span class="d-sm-block d-none">@lang('app.send_link')</span>
                                        <svg width="10" height="10" class="d-sm-none">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#arrow-top-right"></use>
                                        </svg>
                                    </button>
                                </div>
                                <p class="hero__searchbox-text">@lang('app.click_agree')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <picture>
                        <source
                            srcset="{{asset("assets-frontend/images/banner/banner.webp")}} 1x, {{asset("assets-frontend/images/banner/banner@2x.webp")}} 2x"
                            type="image/webp">
                        <source
                            srcset="{{asset("assets-frontend/images/banner/banner.png")}} 1x, {{asset("assets-frontend/images/banner/banner@2x.png")}} 2x"
                            type="image/png">
                        <img src="{{asset("assets-frontend/images/banner/banner.png")}}"
                             class="d-xl-block hero__img"
                             alt="image description">
                    </picture>
                </div>
            </div>
        </div>
    </section>

    <section class="cards overflow-hidden">
        <div class="container-fluid gx-0">
            <div class="row gx-0">
                <div class="col-lg-6">
                    <div class="text-center card__item black">
                        <h2 class="card__title">@lang('app.smart_card')</h2>
                        <div class="card__img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/cards/primary-card.webp")}} 1x, {{asset("assets-frontend/images/cards/primary-card@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/cards/primary-card.png")}} 1x, {{asset("assets-frontend/images/cards/primary-card@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload"
                                     width="360"
                                     height="231"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/cards/primary-card.png")}}"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="row card__info justify-content-center gy-3">
                            <div class="col-auto card__data">
                                <div class="card__value">5 @lang('app.years')</div>
                                <div class="card__label">@lang('app.validity')</div>
                            </div>

                            <div class="col-auto card__data">
                                <div class="card__value">45 000 @lang('app.sum')</div>
                                <div class="card__label">@lang('app.card_design')</div>
                            </div>

                            <div class="col-auto card__data">
                                <div class="card__value">2%</div>
                                <div class="card__label">@lang('app.cashback')</div>
                            </div>
                        </div>

                        <a class="btn btn-white rounded-5 py-3 px-4">
                            @lang('app.more')
                            <svg width="10" height="10" class="ms-1 align-baseline">
                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-center card__item yellow">
                        <h2 class="card__title text-black">@lang('app.smart_card')</h2>
                        <div class="card__img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/cards/black-card.webp")}} 1x, {{asset("assets-frontend/images/cards/black-card@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/cards/black-card.png")}} 1x, {{asset("assets-frontend/images/cards/black-card@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload"
                                     width="360"
                                     height="231"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/cards/black-card.png")}}"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="row card__info justify-content-center gy-3">
                            <div class="col-auto card__data">
                                <div class="card__value text-black">5 @lang('app.years')</div>
                                <div class="card__label text-black-60">@lang('app.validity')</div>
                            </div>

                            <div class="col-auto card__data">
                                <div class="card__value text-black">45 000 @lang('app.sum')</div>
                                <div class="card__label text-black-60">@lang('app.card_design')</div>
                            </div>

                            <div class="col-auto card__data">
                                <div class="card__value text-black">2%</div>
                                <div class="card__label text-black-60">@lang('app.cashback')</div>
                            </div>
                        </div>

                        <a class="btn btn-white rounded-5 py-3 px-4">
                            @lang('app.more')
                            <svg width="10" height="10" class="ms-1 align-baseline">
                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="service__list">
        <div class="container">
            <h3 class="service__list-title mb-5 mb-sm-7 mb-xl-10">@lang('app.bank_services')</h3>

            <section class="section right transfers">
                <div class="section__content position-relative">
                    <div class="row gy-4">
                        <div class="col-lg-5">
                            <div>
                                <h4 class="section__title mb-3">@lang('app.translations')</h4>
                                <p class="section__text mb-5 mb-sm-7 mb-lg-12">@lang('app.which_method_suits')</p>
                                <a href="#!" class="btn btn-primary rounded-5 py-3 px-4">
                                    @lang('app.more')
                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-7">
                            <div class="text-center">
                                <picture>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner2.webp")}} 1x, {{asset("assets-frontend/images/banner/banner2@2x.webp")}} 2x"
                                        type="image/webp"/>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner2.png")}} 1x, {{asset("assets-frontend/images/banner/banner2@2x.png")}} 2x"
                                        type="image/png"/>
                                    <img class="lazyload img-fluid section__img"
                                         loading="lazy"
                                         data-src="{{asset("assets-frontend/images/banner/banner2.png")}}"
                                         alt="image description"/>
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section left payments">
                <div class="section__content position-relative">
                    <div class="row gy-4">
                        <div class="col-lg-7 col-xl-6 order-1 order-lg-0">
                            <div class="text-lg-start text-center">
                                <picture>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner4.webp")}} 1x, {{asset("assets-frontend/images/banner/banner4@2x.webp")}} 2x"
                                        type="image/webp"/>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner4.png")}} 1x, {{asset("assets-frontend/images/banner/banner4@2x.png")}} 2x"
                                        type="image/png"/>
                                    <img class="lazyload img-fluid section__img"
                                         loading="lazy"
                                         data-src="{{asset("assets-frontend/images/banner/banner4.png")}}"
                                         alt="image description"/>
                                </picture>
                            </div>
                        </div>

                        <div class="col-lg-5 col-xl-6 order-0 order-lg-1">
                            <div>
                                <h4 class="section__title mb-3">@lang('app.payments')</h4>
                                <p class="section__text mb-5 mb-sm-7 mb-lg-12">@lang('app.pay_by_qr')</p>
                                <a href="#!" class="btn btn-primary rounded-5 py-3 px-4">
                                    @lang('app.more')
                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section right international-transfers">
                <div class="section__content position-relative">
                    <div class="row gy-4">
                        <div class="col-lg-6">
                            <div>
                                <h4 class="section__title mb-3">@lang('app.international_transfers')</h4>
                                <p class="section__text mb-5 mb-sm-7 mb-lg-12">@lang('app.profitable_transfer')</p>
                                <a href="#!" class="btn btn-primary rounded-5 py-3 px-4">
                                    @lang('app.more')
                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="text-center">
                                <picture>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner5.webp")}} 1x, {{asset("assets-frontend/images/banner/banner5@2x.webp")}} 2x"
                                        type="image/webp"/>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner5.png")}} 1x, {{asset("assets-frontend/images/banner/banner5@2x.png")}} 2x"
                                        type="image/png"/>
                                    <img class="lazyload img-fluid section__img"
                                         loading="lazy"
                                         data-src="{{asset("assets-frontend/images/banner/banner5.png")}}"
                                         alt="image description"/>
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section left deposits">
                <div class="section__content position-relative">
                    <div class="row gy-4">
                        <div class="col-lg-7 col-xl-6 order-1 order-lg-0">
                            <div class="text-lg-start text-center">
                                <picture>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner3.webp")}} 1x, {{asset("assets-frontend/images/banner/banner3@2x.webp")}} 2x"
                                        type="image/webp"/>
                                    <source
                                        data-srcset="{{asset("assets-frontend/images/banner/banner3.png")}} 1x, {{asset("assets-frontend/images/banner/banner3@2x.png")}} 2x"
                                        type="image/png"/>
                                    <img class="lazyload img-fluid section__img"
                                         loading="lazy"
                                         data-src="{{asset("assets-frontend/images/banner/banner3.png")}}"
                                         alt="image description"/>
                                </picture>
                            </div>
                        </div>

                        <div class="col-lg-5 col-xl-6 order-0 order-lg-1">
                            <div>
                                <h4 class="section__title mb-3">@lang('app.contributions')</h4>
                                <p class="section__text mb-5 mb-sm-7 mb-lg-12">@lang('app.withdraw_min_balance')</p>
                                <a href="#!" class="btn btn-primary rounded-5 py-3 px-4">
                                    @lang('app.more')
                                    <svg width="10" height="10" class="ms-1 align-baseline">
                                        <use
                                            xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <section class="shopping">
        <div class="container">
            <h3 class="service__list-title mb-8">@lang('app.shop_market')</h3>
            <div class="row g-4 g-xxl-5">
                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/tv.webp")}} 1x, {{asset("assets-frontend/images/categories/tv@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/tv.png")}} 1x, {{asset("assets-frontend/images/categories/tv@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/tv.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.tv')/@lang('app.video')/@lang('app.audio')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/dryer.webp")}} 1x, {{asset("assets-frontend/images/categories/dryer@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/dryer.png")}} 1x, {{asset("assets-frontend/images/categories/dryer@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/dryer.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.beauty_health')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/cleaner.webp")}} 1x, {{asset("assets-frontend/images/categories/cleaner@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/cleaner.png")}} 1x, {{asset("assets-frontend/images/categories/cleaner@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/cleaner.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.appliances')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/gas.webp")}} 1x, {{asset("assets-frontend/images/categories/gas@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/gas.png")}} 1x, {{asset("assets-frontend/images/categories/gas@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/gas.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.kitchen_appliances')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/book.webp")}} 1x, {{asset("assets-frontend/images/categories/book@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{(asset("assets-frontend/images/categories/book.png"))}} 1x, {{asset("assets-frontend/images/categories/book@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/book.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.for_learning')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/drill.webp")}} 1x, {{asset("assets-frontend/images/categories/drill@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/drill.png")}} 1x, {{asset("assets-frontend/images/categories/drill@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/drill.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.tools')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/phone.webp")}} 1x, {{asset("assets-frontend/images/categories/phone@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/phone.png")}} 1x, {{asset("assets-frontend/images/categories/phone@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/phone.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.phones_gadgets')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/sofa.webp")}} 1x, {{asset("assets-frontend/images/categories/sofa@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/sofa.png")}} 1x, {{asset("assets/images/categories/sofa@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/sofa.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.furniture')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/pomade.webp")}} 1x, {{asset("assets-frontend/images/categories/pomade@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/pomade.png")}} 1x, {{asset("assets-frontend/images/categories/pomade@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/pomade.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.cosmetic_perfumery')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/case.webp")}} 1x, {{asset("assets-frontend/images/categories/case@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/case.png")}} 1x, {{asset("assets-frontend/images/categories/case@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/case.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.computers_office_equipment')</div>
                    </div>
                </div>

                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/chair.webp")}} 1x, {{asset("assets-frontend/images/categories/chair@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/chair.png")}} 1x, {{asset("assets-frontend/images/categories/chair@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset("assets-frontend/images/categories/chair.png")}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.home_office')</div>
                    </div>
                </div>


                <div class="col-6 col-md-4 col-lg-3 col-xxl-2">
                    <div class="category__item text-center h-100 position-relative">
                        <a href="#!" class="ripple-link position-absolute top-0 end-0 bottom-0 start-0"></a>
                        <div class="category__item-img">
                            <picture>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/motor.webp")}} 1x, {{asset("assets-frontend/images/categories/motor@2x.webp")}} 2x"
                                    type="image/webp"/>
                                <source
                                    data-srcset="{{asset("assets-frontend/images/categories/motor.png")}} 1x, {{asset("assets-frontend/images/categories/motor@2x.png")}} 2x"
                                    type="image/png"/>
                                <img class="lazyload img-fluid"
                                     loading="lazy"
                                     data-src="{{asset('assets-frontend/images/categories/motor.png')}}"
                                     width="109" height="109"
                                     alt="image description"/>
                            </picture>
                        </div>
                        <div class="category__item-title">@lang('app.auto_products')</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="news">
        <div class="container">
            <div class="d-flex align-items-baseline mb-5 mb-lg-8">
                <h3 class="service__list-title me-5">@lang('app.news_events')</h3>
                <a href="{{route('blog.index')}}"
                   class="text-primary text-decoration-underline fw-bold d-none d-lg-inline-block">
                    Смотреть все новости</a>
            </div>
            <div class="news__swiper swiper--lazy swiper">
                <div class="swiper-wrapper">
                    @foreach($blogs as $blog)
                        <div class="swiper-slide">
                            <div class="news__item">
                                <div class="news__item-header">
                                    <div class="d-flex align-items-center justify-content-between mb-3">
                                        <div class="news__item-category">@lang('app.news')</div>
                                        <div class="news__item-time">
                                            <svg width="13" height="18" class="me-1">
                                                <use
                                                    xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#calendar"></use>
                                            </svg>
                                            {{$blog->created_at->format('d.m.Y')}}
                                        </div>
                                    </div>
                                    <h4 class="news__item-title text__three-line mb-3">
                                        <a href="{{route('blog.view',['blog' => $blog->id])}}" class="border-link">
                                            {{$blog->{'title_' . app()->getLocale()} }}
                                        </a>
                                    </h4>
                                </div>
                                <a href="{{route('blog.view',['blog' => $blog->id])}}"
                                   class="ripple-link news__item-img mb-5">
                                    <picture>
                                        <source
                                            data-srcset="{{ asset('storage/'.$blog->image_path_1) }} 1x, {{ asset('storage/'.$blog->image_path_2) }} 2x"
                                            type="image/png"/>
                                        <img class="lazyload rounded-4 img-fluid"
                                             loading="lazy"
                                             width="348"
                                             height="200"
                                             data-src="{{ asset('storage/'.$blog->image_path_1) }}"
                                             alt="image description"/>
                                    </picture>
                                </a>
                                <div class="news__item-footer">
                                    <p class="news__item-text text__three-line">
                                        {!! $blog->{'short_description_' . app()->getLocale()} !!}
                                    </p>
                                    <a href="{{route('blog.view',['blog' => $blog->id])}}"
                                       class="news__item-button btn btn-primary rounded-5 py-2 px-4">
                                        Читать далее
                                        <svg width="10" height="10" class="ms-1 align-baseline">
                                            <use
                                                xlink:href="{{("assets-frontend/images/smrtbicons.svg")}}#angle-right"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination position-static mt-3"></div>
            </div>
            <div class="text-center d-lg-none mt-5">
                <a href="{{route('blog.index')}}" class="text-primary text-decoration-underline fw-bold">
                    @lang('app.see_all_news')</a>
            </div>
        </div>
    </section>
@endsection
