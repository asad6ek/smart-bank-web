<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SmartBank</title>
    <link rel="icon" href="{{asset("assets-frontend/images/favicon.ico")}}">
    <link rel="stylesheet" href="{{asset("assets-frontend/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets-frontend/css/font-manrope.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets-frontend/vendor/css/swiper-bundle.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets-frontend/vendor/css/waves.css")}}">
    <link rel="stylesheet" href="{{asset("assets-frontend/css/main.min.css")}}">

</head>
<body>
@include('frontend.layouts.header')
@yield('contend')
@include('frontend.layouts.footer')
<script src="{{asset("assets-frontend/vendor/js/jquery.min.js")}}"></script>
<script src="{{asset("assets-frontend/vendor/js/bootstrap.bundle.min.js")}}"></script>
<script src="{{asset("assets-frontend/vendor/js/waves.min.js")}}"></script>
<script src="{{asset("assets-frontend/vendor/js/lazyload.min.js")}}" async></script>
<script src="{{asset("assets-frontend/vendor/js/swiper-bundle.min.js")}}"></script>
<script src="{{asset("assets-frontend/js/main.min.js")}}"></script>
@stack('scripts')
</body>
</html>
