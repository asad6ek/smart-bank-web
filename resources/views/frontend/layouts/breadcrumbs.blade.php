<div class="border-top border-bottom py-4 breadcrumb-scroll">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb flex-nowrap align-items-center mb-0">
                <li class="breadcrumb-item">
                    <a href="{{route('site.index')}}">
                        <svg width="14" height="14" class="me-1 align-baseline">
                            <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#home"></use>
                        </svg>
                    </a>
                </li>
                @foreach($breadcrumbs as $breadcrumb )
                    @if(isset($breadcrumb['route']))
                        <li class="breadcrumb-item">
                            <a href="{{$breadcrumb['route']}}">
                                {{$breadcrumb['name']}}
                            </a>
                        </li>
                    @else
                        <li class="breadcrumb-item fw-semibold active" aria-current="page">
                            {{$breadcrumb['name']}}
                        </li>
                    @endif

                @endforeach
            </ol>
        </nav>
    </div>
</div>
