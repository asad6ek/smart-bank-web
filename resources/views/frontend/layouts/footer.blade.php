<footer>
    <div class="footer__top">
        <div class="container">
            <div class="row gy-3 justify-content-between">
                <div class="col-lg-5 col-xxl-4">
                    <div class="d-flex flex-column flex-lg-row align-items-lg-center">
                        <p class="footer__text mb-3 mb-lg-0 me-lg-4 w-50">@lang('app.download_mobile_app')</p>
                        <div class="row">
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#store"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#play"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-xl-6 col-xxl-5">
                    <div class="d-flex flex-column flex-lg-row align-items-lg-center justify-content-lg-end">
                        <p class="footer__text mb-3 mb-lg-0 me-lg-4">@lang('app.we_social_net')</p>
                        <div class="row">
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#tg"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#fb"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#ig"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                            <div class="col-auto">
                                <noindex>
                                    <a href="#" rel="nofollow" class="btn btn-secondary p-0 social__btn"
                                       target="_blank">
                                        <svg width="18" height="18">
                                            <use
                                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#tw"></use>
                                        </svg>
                                    </a>
                                </noindex>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer__bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="row gy-3">
                        <div class="col-lg-4 col-xl-3">
                            <div>
                                <h4 class="footer__list-title mb-3">@lang('app.private_individuals')</h4>
                                <ul class="footer__list list-unstyled mb-0">
                                    <li><a class="border-link" href="#">@lang('app.cards')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.accounts')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.translations')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.payments')</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-3">
                            <div>
                                <h4 class="footer__list-title mb-3">@lang('app.private_individuals')</h4>
                                <ul class="footer__list list-unstyled mb-0">
                                    <li><a class="border-link" href="#">@lang('app.cards')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.accounts')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.translations')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.payments')</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-3">
                            <div>
                                <h4 class="footer__list-title mb-3">@lang('app.private_individuals')</h4>
                                <ul class="footer__list list-unstyled mb-0">
                                    <li><a class="border-link" href="#">@lang('app.cards')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.accounts')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.translations')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.payments')</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-3">
                            <div>
                                <h4 class="footer__list-title mb-3">@lang('app.private_individuals')</h4>
                                <ul class="footer__list list-unstyled mb-0">
                                    <li><a class="border-link" href="#">@lang('app.cards')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.accounts')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.translations')</a></li>
                                    <li><a class="border-link" href="#">@lang('app.payments')</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="useful__data">
                        <ul class="footer__list-v2 list-unstyled mb-0">
                            <li>
                                <a class="border-link" href="tel:1216">1216</a>
                                <p class="footer__text mb-0">@lang('app.for_free_calls')</p>
                            </li>
                            <li>
                                <a class="border-link" href="tel:+998911660680">+998 91 166 06
                                    80</a>
                                <p class="footer__text mb-0">@lang('app.calls_abroad')</p>
                            </li>
                            <li>
                                <address class="footer__text mb-0 text-black"><a
                                        href="https://yandex.uz/maps/-/CCURzRGksB" target="_blank" class="border-link">@lang('app.address')</a></address>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="footer__copyright">
        <div class="container">
            <div class="row gy-4 align-items-center">
                <div class="col-lg-4">
                    <div class="footer__text text-black">© 1997—2022, SmartBank</div>
                    <p class="footer__text mb-0">@lang('app.general_license') №1000</p>
                </div>

                <div class="col-md-6 col-lg-5">
                    <div class="footer__text text-black">@lang('app.last_update'):</div>
                    <p class="footer__text mb-0">26 @lang('app.july') 2022, 09:53 (GMT+5)&#x1F815;</p>
                </div>

                <div class="col-md-6 col-lg-3 text-md-end">
                    <button class="btn btn-white rounded-btn">@lang('app.found_mistake')</button>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"></path>
    </svg>
</div>

<div class="offcanvas offcanvas-top search__top" tabindex="-1" id="offcanvasTop" aria-labelledby="offcanvasTopLabel">
    <div class="offcanvas-header">
        <div class="container">
            <div class="row gx-3 align-items-center">
                <div class="col-auto d-none d-xl-block">
                    <a href="/" class="navbar-brand py-0 pe-4">
                        <img src="{{asset("assets-frontend/images/logo-black.svg")}}" alt="logo" width="149">
                    </a>
                </div>
                <div class="col">
                    <form class="search__top-form">
                        <input type="text"
                               class="form-control border-0"
                               placeholder="@lang('app.search')...">
                    </form>
                </div>
                <div class="col-auto">
                    <button class="btn btn btn-dark rounded-4 py-2 px-3"
                            type="button"
                            data-bs-dismiss="offcanvas" aria-label="Close">
                        <svg width="9" height="9">
                            <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#times"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
