<header class="fixed-top @if(!request()->routeIs('site.index')) bg-white @endif">
    <div class="header__top">
        <div class="container">
            <nav class="row align-items-center position-relative">
                <div class="col-6 col-xl-8">
                    <div class="d-flex align-items-center">
                        <a href="{{route('site.index')}}" class="navbar-brand py-0 me-4 pe-4">
                            <picture>
                                @if(request()->routeIs('site.index'))
                                    <source media="(min-width: 1200px)"
                                            srcset="{{asset("assets-frontend/images/logo-white.svg")}}">
                                @endif
                                <img src="{{asset("assets-frontend/images/logo-black.svg")}}" alt="logo" width="149">
                            </picture>
                        </a>

                        <ul class="navbar-nav flex-row justify-content-between d-none d-xl-flex">
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('site*')) active @endif " href="/">
                                    @lang('app.private_individuals')
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.business')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('about*')) active @endif "
                                   href="{{route('about.index')}}">
                                    @lang('app.about_bank')
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('blog*')) active @endif " href="{{route('blog.index')}}">
                                    @lang('app.press_center')
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-6 col-xl-4">
                    <div class="d-flex align-items-center justify-content-end header__top-actions">
                        <button
                            class=" @if(!request()->routeIs('site.index')) btn-secondary @else btn-outline-light @endif  btn__no-text btn rounded-circle p-2 d-flex align-items-center justify-content-center"
                            type="button"
                            data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasTop"
                            aria-controls="offcanvasTop">
                            <svg width="12" height="12">
                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#search"></use>
                            </svg>
                        </button>

                        <button
                            class="btn__no-text btn btn-dark rounded-circle p-2 d-flex align-items-center justify-content-center me-0 d-xl-none"
                            type="button"
                            data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasMenu"
                            aria-controls="offcanvasMenu">
                            <svg width="12" height="10">
                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#burger"></use>
                            </svg>
                        </button>

                        <div
                            class=" @if(!request()->routeIs('site.index')) lang__dropdown @endif dropdown d-none d-xl-block">
                            <button
                                class="btn @if(!request()->routeIs('site.index')) btn-secondary @else btn-outline-light @endif rounded-btn dropdown-toggle"
                                type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{\App\Constant\LangConstant::getLang(app()->getLocale())}}
                            </button>
                            <ul class="dropdown-menu">
                                @foreach(\App\Constant\LangConstant::getInactiveLangs(app()->getLocale()) as $key => $lang)
                                 <li>
                                     <a class="dropdown-item ripple-link"
                                        href="{{route('changeLang',['lang' => $key])}}">
                                         {{$lang}}
                                     </a>
                                 </li>
                                @endforeach
                            </ul>
                        </div>

                        <a href="tel:1216"
                           class="phone__btn d-none d-xl-inline-block btn btn-dark text-white rounded-btn m-0"
                           type="button">
                            <svg width="12" height="12" class="me-2">
                                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#phone"></use>
                            </svg>
                            <span>1216</span>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    @if(request()->routeIs('site*'))
        <div class="header__bottom d-none d-xl-block">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-8">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('site.card')) active @endif "
                                   href="{{route('site.card')}}">@lang('app.cards')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.accounts')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.translations')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.payments')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('site.deposit')) active @endif "
                                   href="{{route('site.deposit')}}">@lang('app.contributions')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.my_home')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.support')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-auto">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link" href="#">SmartMarket</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.net_bank')</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @elseif(request()->routeIs('about*'))
        <div class="header__bottom d-none d-xl-block">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-8">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('about.index')) active @endif"
                                   href="{{route('about.index')}}">
                                    @lang('app.bank_inf')
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('about.management')) active @endif"
                                   href="{{route('about.management')}}">@lang('app.management')</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('about.partner')) active @endif"
                                   href="{{route('about.partner')}}">@lang('app.partners')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('about.vacancy')) active @endif"
                                   href="{{route('about.vacancy')}}">@lang('app.jobs')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-auto">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.net_bank')</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    @elseif(request()->routeIs('blog*'))
        <div class="header__bottom d-none d-xl-block">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-8">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('blog.index')) active @endif"
                                   href="{{route('blog.index')}}">@lang('app.news')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.tenders')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.articles')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.events')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(request()->routeIs('site.deposit')) active @endif "
                                   href="{{route('site.deposit')}}">@lang('app.contributions')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.smi')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-auto">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item">
                                <a class="nav-link" href="#">@lang('app.net_bank')</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

</header>
<div class="offcanvas offcanvas-end offcanvasMenu" tabindex="-1" id="offcanvasMenu">
    <div class="offcanvas-header border-bottom">
        <a href="/" class="navbar-brand py-0">
            <img src="{{asset("assets-frontend/images/logo-black.svg")}}" alt="logo" width="149">
        </a>
        <button class="btn btn btn-dark rounded-4 py-2 px-3"
                type="button"
                data-bs-dismiss="offcanvas"
                aria-label="Close">
            <svg width="9" height="10">
                <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#times"></use>
            </svg>
        </button>
    </div>
    <div class="offcanvas-body">
        <div class="pb-0 mb-4">
            <div class="row gx-3">
                <div class="col-7">
                    <div class="lang__dropdown dropdown">
                        <button
                            class="btn btn-secondary w-100 rounded-btn dropdown-toggle py-2"
                            type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{\App\Constant\LangConstant::getLang(app()->getLocale())}}
                        </button>
                        <ul class="dropdown-menu">
                            @foreach(\App\Constant\LangConstant::getInactiveLangs(app()->getLocale()) as $key => $lang)
                                <li>
                                    <a class="dropdown-item ripple-link"
                                       href="{{route('changeLang',['lang' => $key])}}">
                                        {{$lang}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-5">
                    <a href="tel:1216"
                       class="phone__btn py-2 w-100 btn btn-dark text-white rounded-btn m-0">
                        <svg width="12" height="12" class="me-2">
                            <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#phone"></use>
                        </svg>
                        <span>1216</span>
                    </a>
                </div>
            </div>
        </div>
        <div>
            <div class="offcanvasMenu__item">
                <a
                    class="offcanvasMenu__item-title text-decoration-none d-inline-block mb-3 @if(request()->routeIs('site*')) active @endif "
                    href="/"> @lang('app.private_individuals') </a>

                <ul class="list-unstyled">
                    <li>
                        <a class="text-decoration-none" href="{{route('site.card')}}">@lang('app.cards')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{route('about.index')}}">@lang('app.about_bank')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{'about.management'}}">@lang('app.management')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{route("about.partner")}}">@lang('app.partners')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{route("site.deposit")}}">@lang('app.contributions')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{route("about.vacancy")}}">@lang('app.jobs')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="{{("blog.index")}}">@lang('app.news')</a>
                    </li>
                </ul>
            </div>

            <div class="offcanvasMenu__item">
                <a href="#" class="offcanvasMenu__item-title text-decoration-none d-inline-block mb-3">Частным лицам</a>
                <ul class="list-unstyled">
                    <li>
                        <a class="text-decoration-none" href="{{route('site.card')}}">@lang('app.cards')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.accounts')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.translations')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.payments')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.contributions')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.my_home')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.support')</a>
                    </li>
                </ul>
            </div>

            <div class="offcanvasMenu__item">
                <a href="#" class="offcanvasMenu__item-title text-decoration-none d-inline-block mb-3">Частным лицам</a>
                <ul class="list-unstyled">
                    <li>
                        <a class="text-decoration-none" href="{{route('site.card')}}">@lang('app.cards')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.accounts')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.translations')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.payments')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.contributions')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.my_home')</a>
                    </li>
                    <li>
                        <a class="text-decoration-none" href="#">@lang('app.support')</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
