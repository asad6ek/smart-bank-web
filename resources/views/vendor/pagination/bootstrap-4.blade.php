@if ($paginator->hasPages())
    <nav>
        <ul class="pagination align-items-center justify-content-end">
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled bot" aria-disabled="true"><span class="page-link border-0">{{ $element }}</span>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link border-0">{{ $page }}</span>
                            </li>
                        @else
                            <li class="page-item"><a class="page-link border-0" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <a class="page-link border-0" href="#" aria-label="Previous">
                        <svg width="22" height="22">
                            <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#arrow-left"></use>
                        </svg>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                       aria-label="@lang('pagination.previous')">
                        <svg width="22" height="22">
                            <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#arrow-left"></use>
                        </svg>
                    </a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"
                       aria-label="@lang('pagination.next')">
                        <svg width="22" height="22">
                            <use
                                xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#arrow-right"></use>
                        </svg>
                    </a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <svg width="22" height="22">
                        <use xlink:href="{{asset("assets-frontend/images/smrtbicons.svg")}}#arrow-right"></use>
                    </svg>
                </li>
            @endif
        </ul>
    </nav>
@endif
