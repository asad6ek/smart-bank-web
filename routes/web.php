<?php

use App\Http\Controllers\Frontend\{AboutController, BlogController, SiteController};
use App\Constant\LangConstant;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('language_manger')->group(function () {
    Route::get('/', array(SiteController::class, 'index'))->name('site.index');
    Route::get('/card', array(SiteController::class, 'card'))->name('site.card');
    Route::post('/send-application', [SiteController::class, 'contact'])->name('site.send-application');
    Route::get('/deposit', array(SiteController::class, 'deposit'))->name('site.deposit');
    Route::get('/about', array(AboutController::class, 'index'))->name('about.index');
    Route::get('/management', array(AboutController::class, 'management'))->name('about.management');
    Route::get('/partner', array(AboutController::class, 'partner'))->name('about.partner');
    Route::get('/vacancy', array(AboutController::class, 'vacancy'))->name('about.vacancy');
    Route::get('/blog', array(BlogController::class, 'index'))->name('blog.index');
    Route::get('/blog/{blog}', array(BlogController::class, 'view'))->name('blog.view');
});

Route::get('lang/{lang}', function ($lang) {
    if (LangConstant::getLang($lang)) {
        App::setLocale($lang);
        session()->put('locale', $lang);
    }
    return redirect()->back();
})->name('changeLang');

